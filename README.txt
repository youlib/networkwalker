The purpose of this software is to scan java source code
and deduct which members use which, producing a network of members.
Furthermore, it produces a network of cross-class dependencies.
Antlr has been used for the parsing of the java files.
