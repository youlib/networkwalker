package uk.ac.open.t802.network.walker.domain;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

/**
 * Author youlib
 * Since version 1.0
 */
public class Graph {

    public Map<String, String> bounds = Maps.newHashMap();
    public Map<String, Set<String>> interfaces = Maps.newHashMap();
    public Map<String, Map<String, String>> variableMaps = Maps.newHashMap();
    private StringBuilder loggingStringBuilder = new StringBuilder();
    private StringBuilder activityStringBuilder = new StringBuilder();
    private StringBuilder failedEdgeStringBuilder = new StringBuilder();

    public void addInterface(String implementingClass, String interfaceName) {
        if (interfaces.containsKey(implementingClass)) {
            interfaces.get(implementingClass).add(interfaceName);
        } else {
            Set<String> set = Sets.newHashSet(interfaceName);
            interfaces.put(implementingClass, set);
        }
    }

    public void addExtents(String className, String extendingClass) {
        bounds.put(className, extendingClass);
    }

    public void addVariable(String className, String variable, String type) {
        if (!StringUtils.isEmpty(className)) {
            if (variableMaps.containsKey(className)) {
                variableMaps.get(className).put(variable, ParseUtils.normaliseType(type));
            } else {
                Map<String, String> variables = Maps.newHashMap();
                variables.put(variable, ParseUtils.normaliseType(type));
                variableMaps.put(className, variables);
            }
        }

    }

    public String findVariable(String className, String variable) {
        if (StringUtils.isEmpty(variable)) return null;
        if (Character.isUpperCase(variable.charAt(0))) return variable;
        String type = getVariableForClass(className, variable);
        if (type == null) {
            LinkedList<String> inheritanceList = generateInheritanceList(className);
            for (String parent : inheritanceList) {
                type = getVariableForClass(parent, variable);
                if (type != null) {
                    return type;
                }
            }
        }
        return type;
    }

    protected LinkedList<String> generateInheritanceList(String className) {
        LinkedList<String> list = Lists.newLinkedList();
        String node = className;
        list.add(className);
        while (bounds.containsKey(node)) {
            list.add(bounds.get(node));
            node = list.getLast();
        }
        return list;
    }

    private String getVariableForClass(String className, String variable) {
        if (variableMaps.containsKey(className) && variableMaps.get(className).containsKey(variable)) {
            return variableMaps.get(className).get(variable);
        }
        return null;
    }

    public void appendLogging(String message, Member member) {
        if (message.startsWith("POINT")) {
            activityStringBuilder.append(message);
            activityStringBuilder.append("\n");
        } else {
            loggingStringBuilder.append(message);
            loggingStringBuilder.append(" : ");
            loggingStringBuilder.append(member);
            loggingStringBuilder.append("\n");
        }
    }

    public void appendLogging(String message, JavaClass javaClass) {
        loggingStringBuilder.append(message);
        loggingStringBuilder.append(" : ");
        loggingStringBuilder.append(javaClass);
        loggingStringBuilder.append("\n");
    }

    public String getLogging() {
        return loggingStringBuilder.toString();
    }

    public String getActions() {
        return activityStringBuilder.toString();
    }

    public void appendFailedEdge(String message) {
        failedEdgeStringBuilder.append(message);
        failedEdgeStringBuilder.append("\n");
    }

    public String getFailedEdges() {
        return failedEdgeStringBuilder.toString();
    }

}
