package uk.ac.open.t802.network.walker.domain;

import com.google.common.collect.Sets;
import org.apache.commons.lang3.Validate;
import java.util.Set;

/**
 * Author youlib
 * Since version 1.0
 */
public class JavaClass implements Comparable<JavaClass> {

    public final Set<Member> members = Sets.newHashSet();
    public final String packageName;
    public final String className;
    public int id;
    public Color color;

    public JavaClass(String packageName, String name, Color color) {
        Validate.notEmpty(packageName);
        Validate.notEmpty(name);
        Validate.notNull(color);
        this.packageName = packageName;
        this.className = name;
        this.color = color;
    }

    public void addMember(Member member) {
        members.add(member);

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JavaClass javaClass = (JavaClass) o;

        if (!packageName.equals(javaClass.packageName)) return false;
        return className.equals(javaClass.className);

    }

    @Override
    public int compareTo(JavaClass o) {
        return Integer.compare(this.id, o.id);
    }

    @Override
    public int hashCode() {
        int result = packageName.hashCode();
        result = 31 * result + className.hashCode();
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(+id);
        sb.append(":" + packageName);
        sb.append(":" + className);
        sb.append(":" + color);
        return sb.toString();
    }
}
