package uk.ac.open.t802.network.walker.domain;

import org.apache.commons.lang3.StringUtils;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Author youlib
 * Since version 1.0
 */
public class ParseUtils {

    public static List<String> normaliseType(List<String> arguments) {
        if (arguments != null) {
            return arguments.stream().map(argument ->
                normaliseType(argument)).collect(Collectors.toList());
        }
        return arguments;
    }

    public static String normaliseType(String name) {
        if (StringUtils.isEmpty(name)) {
            return name;
        }

        while (name.contains("char")) {
            name = name.replace("char", "Char");

        }
        while (name.contains("boolean")) {
            name = name.replace("boolean", "Boolean");
        }
        while (name.contains("float")) {
            name = name.replace("float", "Float");
        }
        while (name.contains("int")) {
            name = name.replace("int", "Integer");
        }
        while (name.contains("long")) {
            name = name.replace("long", "Long");
        }
        while (name.contains("double")) {
            name = name.replace("double", "Double");
        }
        while (name.contains("null")) {
            name = name.replace("null", "Null");
        }

        return name;
    }

    public static String normaliseClass(String className) {

        if (!StringUtils.isEmpty(className) && className.contains("<")) {
            return className.substring(0, className.indexOf("<"));
        }
        return className;
    }

    public static String getType(String collectionVariable) {
        if (!StringUtils.isEmpty(collectionVariable) && collectionVariable.contains("<") && collectionVariable.contains(">")) {
            return collectionVariable.substring(collectionVariable.indexOf("<") + 1, collectionVariable.indexOf(">"));
        }
        return collectionVariable;
    }
}
