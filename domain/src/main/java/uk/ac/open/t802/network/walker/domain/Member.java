package uk.ac.open.t802.network.walker.domain;

import org.apache.commons.lang3.Validate;
import java.util.List;

/**
 * @author Georgia Bogdanou
 */
public class Member implements Comparable<Member> {

    public String className;
    public String returnType;
    public String methodName;
    public List<String> methodArguments;
    public int id;
    public Color color;

    private Member(String className, String returnType, String methodName, List<String> methodArguments, Color color) {

        returnType = ParseUtils.normaliseType(returnType);
        methodName = ParseUtils.normaliseType(methodName);
        methodArguments = ParseUtils.normaliseType(methodArguments);
        this.returnType = returnType;
        this.className = className;
        this.methodName = methodName;
        this.methodArguments = methodArguments;
        this.color = color;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Member member = (Member) o;

        if (!className.equals(member.className)) return false;
        if (!methodName.equals(member.methodName)) return false;
        return methodArguments.equals(member.methodArguments);

    }

    @Override
    public int hashCode() {
        int result = className.hashCode();
        result = 31 * result + methodName.hashCode();
        result = 31 * result + methodArguments.hashCode();
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(+id);
        sb.append(":" + className);
        sb.append(":" + returnType);
        sb.append(":" + methodName);
        sb.append(":" + methodArguments);
        sb.append(":" + color);
        return sb.toString();
    }

    @Override
    public int compareTo(Member o) {
        return Integer.compare(this.id, o.id);
    }

    public static class Builder {

        private String className;
        private String returnType;
        private String methodName;
        private List<String> methodArguments;
        private Color color;

        public Builder(Color color) {
            this.color = color;
        }

        public Builder withClassName(String className) {
            this.className = className;
            return this;
        }

        public Builder withReturnType(String returnType) {
            this.returnType = returnType;
            return this;
        }

        public Builder withMethodName(String methodName) {
            this.methodName = methodName;
            return this;
        }

        public Builder withMethodArguments(List<String> methodArguments) {
            this.methodArguments = methodArguments;
            return this;
        }

        public Member build() {

            Validate.notEmpty(this.className);
            Validate.notEmpty(this.returnType);
            Validate.notEmpty(this.methodName);
            Validate.notNull(this.color);
            Validate.notNull(this.methodArguments);
            return new Member(this.className, this.returnType, this.methodName, this.methodArguments, this.color);
        }

        public Member template() {
            return new Member(this.className, this.returnType, this.methodName, this.methodArguments, this.color);

        }
    }
}
