package uk.ac.open.t802.network.walker.domain;

/**
 * @author Georgia Bogdanou
 */
public class NotInScopeException extends RuntimeException {

    public NotInScopeException(String type) {
        super("The following type is not in scope" + type);
    }
}
