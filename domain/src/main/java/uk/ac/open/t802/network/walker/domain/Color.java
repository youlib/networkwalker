package uk.ac.open.t802.network.walker.domain;

/**
 * @author Georgia Bogdanou
 */
public enum Color {
    RED("#FF3333"),
    ORANGE("#FF9933"),
    GREEN("#99CC77"),
    BLUE("#66B2FF");


    private String rgbValue;

    Color(String rgbValue) {
        this.rgbValue = rgbValue;
    }

    public String getRgbValue() {
        return rgbValue;
    }
}
