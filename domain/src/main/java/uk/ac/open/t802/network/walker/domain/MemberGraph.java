package uk.ac.open.t802.network.walker.domain;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import java.util.*;

/**
 * @author Georgia Bogdanou
 */
public class MemberGraph {

    public Graph graph;
    protected final IdGenerator idGenerator;

    private Boolean multigraph;
    private Set<Member> nodes = Sets.newTreeSet();
    private Map<Member, List<Member>> edges = Maps.newHashMap();

    public MemberGraph(IdGenerator idGenerator, Graph graph, Boolean multigraph) {
        this.graph = graph;
        this.idGenerator = idGenerator;
        this.multigraph = multigraph;
    }

    public Member addNode(Member node) {
        Validate.notNull(node, "cannot add null node");
        Member existingNode = getNode(node);
        if (existingNode == null) {
            node.id = idGenerator.generateId();
            node.color = defineNodeColor(node.className);
            nodes.add(node);
            return node;
        }
        return existingNode;
    }

    public void addEdge(Member from, Member to) {
        Validate.notNull(from, "member from cannot be null when adding an edge");
        Validate.notNull(to, "member to cannot be null when adding an edge");
        if (withinScope(from) && withinScope(to)) {
            if (edges.containsKey(from)) {
                if (multigraph) {
                    edges.get(from).add(to);
                }
                else if (!edges.get(from).contains(to)) {
                    edges.get(from).add(to);
                }
            } else {
                List<Member> destinations = Lists.newArrayList();
                destinations.add(to);
                edges.put(from, destinations);
            }
        }
    }


    public boolean withinScope(Member inputMember) {
        Iterator<Member> iterator = nodes.iterator();
        while(iterator.hasNext()) {
            Member member = iterator.next();
            if(areMembersEqual(member, inputMember, true))
                return true;
        }

        return false;
    }

    public Member getNode(Member member) {
        for (Member existingNode : nodes) {
            if (areMembersEqual(existingNode, member, false)) {
                return existingNode;
            }
        }
        return null;
    }

    private boolean areMembersEqual(Member firstMember, Member secondMember, boolean generic) {
        if (firstMember == null || secondMember == null) return false;
        if (!firstMember.className.equals(secondMember.className)) return false;
        if (!firstMember.methodName.equals(secondMember.methodName)) return false;
        return areMethodArgumentsEqual(firstMember.methodArguments, secondMember.methodArguments, generic);
    }

    private boolean areMethodArgumentsEqual(List<String> firstArguments, List<String> secondArguments, boolean generic) {
        if (firstArguments.size() != secondArguments.size()) return false;

        boolean result = true;
        for (int i = 0; i < firstArguments.size(); i++) {
            result = assertArguments(firstArguments.get(i), secondArguments.get(i), generic);
            if (!result) {
                return result;
            }
        }
        return result;

    }

    private boolean assertArguments(String firstArgument, String secondArgument, boolean generic) {
        if (firstArgument == null || secondArgument == null) {
            return false;
        }
        if (secondArgument.equals(firstArgument)) {
            return true;
        }
        if (firstArgument.contains("List") && secondArgument.contains("List")){
            return true;
        }
        if (firstArgument.contains("Map") && secondArgument.contains("Map")){
            return true;
        }
        if (firstArgument.contains("Set") && secondArgument.contains("Set")){
            return true;
        }

        if (!generic) {
            return false;
        }
        if (!secondArgument.contains("?") && !secondArgument.equals(firstArgument)) {
            return true;
        }
        if (firstArgument.equals("?") && !secondArgument.contains("<")) {
            return true;
        }
        if (secondArgument.contains("<?>") && firstArgument.contains("<") && firstArgument.contains(">")) {
            return firstArgument.substring(0, firstArgument.indexOf("<")).equals(secondArgument.substring(0, secondArgument.indexOf("<")));
        }

        return false;
    }

    private Member getGenericNode(Member member) {
        if (member == null) {
            return null;
        }
        for (Member existingNode : nodes) {
            if (existingNode.methodName.equals(member.methodName) && existingNode.className.equals(member.className) && areMethodArgumentsEqual(existingNode.methodArguments, member.methodArguments, true)) {
                return existingNode;

            }
        }
        return null;
    }

    public Member findNode(Member inputMember, boolean inheritance) {
        Member member = null;
        if (inputMember != null && inputMember.className != null){
        if (inheritance) {
            //is it an interface?
            member = findInterfaceNode(inputMember);
        }
        //it is in the same Class?
        if (member == null) {
            member = getNode(inputMember);
        }
        if (member == null) {
            member = getGenericNode(inputMember);
        }
        if (inheritance) {
            //it is not an implementation in the class, go for inheritance
            if (member == null) {
                LinkedList<String> inheritanceList = graph.generateInheritanceList(inputMember.className);
                for (String parent : inheritanceList) {
                    member = getNode(new Member.Builder(Color.BLUE).withClassName(parent).withReturnType(inputMember.returnType).withMethodName(inputMember.methodName).withMethodArguments(inputMember.methodArguments).template());
                    if (member != null) {
                        return member;
                    }
                }

            }
        }
        }
        return member;

    }

    private Member findInterfaceNode(Member inputMember) {

        Member node = null;
        if (inputMember != null && inputMember.className != null && graph.interfaces.containsKey(inputMember.className)) {

            LinkedList<LinkedList<String>> listOfLists = generateInterfaceLists(inputMember.className);
            while (!listOfLists.isEmpty()) {
                LinkedList<String> inheritanceList = listOfLists.removeLast();
                while (!inheritanceList.isEmpty() && inheritanceList.size() > 1) {
                    node = getNode(new Member.Builder(Color.BLUE).withClassName(inheritanceList.removeLast()).withReturnType(inputMember.returnType).withMethodName(inputMember.methodName).withMethodArguments(inputMember.methodArguments).template());
                    if (node != null) {
                        return node;
                    }
                }
            }
        }

        return node;

    }

    private LinkedList<LinkedList<String>> generateInterfaceLists(String className) {
        LinkedList<LinkedList<String>> listOfLists = Lists.newLinkedList();

        List<String> nodeList = Lists.newArrayList(className);

        boolean hasMore = true;
        while (hasMore) {
            boolean foundMore = false;
            List<String> freshNodeList = Lists.newArrayList();

            for (String node : nodeList) {
                List<String> listOfInterfaces = Lists.newArrayList(graph.interfaces.get(node));

                for (String interfaceName : listOfInterfaces) {
                    addToListOfLists(node, interfaceName, listOfLists);
                    if (graph.interfaces.containsKey(interfaceName)) {
                        foundMore = true;
                        freshNodeList.add(interfaceName);
                    }
                }
            }
            nodeList = freshNodeList;

            hasMore = foundMore;
        }

        return listOfLists;
    }

    private void addToListOfLists(String node, String interfaceName, List<LinkedList<String>> listOfLists) {
        LinkedList<String> list = getBeforeLastList(node, listOfLists);
        if (list != null) {
            LinkedList<String> listCopy = Lists.newLinkedList(list);
            listCopy.removeLast();
            listCopy.add(interfaceName);
            listOfLists.add(listCopy);

        } else {
            LinkedList<String> freshList = Lists.newLinkedList();
            freshList.add(node);
            freshList.add(interfaceName);
            listOfLists.add(freshList);
        }

    }

    private LinkedList<String> getBeforeLastList(String node, List<LinkedList<String>> listOfLists) {
        for (LinkedList<String> list : listOfLists) {
            if (!list.getLast().equals(node) && list.contains(node)) {
                return list;
            }
        }

        return null;
    }

    private Color defineNodeColor(String className) {
        Color color = Color.BLUE;
        if (StringUtils.endsWith(className, "Resource")) {
            color = Color.RED;
        }
        if (StringUtils.endsWith(className, "Manager")) {
            color = Color.GREEN;
        }

        if (StringUtils.endsWith(className, "Broker")) {
            color = Color.ORANGE;
        }
        return color;
    }

    public Set<Member> getNodes() {
        return nodes;
    }

    public Map<Member, List<Member>> getEdges() {
        return edges;
    }
}
