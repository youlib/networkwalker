package uk.ac.open.t802.network.walker.domain;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Georgia Bogdanou
 */
public class DependencyGraph {

    private final IdGenerator idGenerator;
    public Graph graph;
    private Boolean multigraph;

    public Set<JavaClass> nodes = Sets.newTreeSet();
    public Map<JavaClass, List<JavaClass>> edges = Maps.newHashMap();

    public DependencyGraph(IdGenerator idGenerator, Graph graph, Boolean multigraph) {
        this.graph = graph;
        this.idGenerator = idGenerator;
        this.multigraph = multigraph;
    }

    public JavaClass addNode(JavaClass node) {
        if (!node.className.contains(".")) {
            JavaClass javaClass = getNode(node);
            if (javaClass == null) {
                node.id = idGenerator.generateId();
                node.color = defineNodeColor(node.className);
                nodes.add(node);
                return node;
            }

            return javaClass;

        }
        return null;
    }

    public void addEdge(JavaClass from, JavaClass to) {
        Validate.notNull(from, "from cannot be null when adding an edge");
        Validate.notNull(to, "to cannot be null when adding an edge");
        if (edges.containsKey(from)) {
            if (multigraph) {
                edges.get(from).add(to);
            }
            else if (!edges.get(from).contains(to)){
                edges.get(from).add(to);
            }
        } else {
            List<JavaClass> destinations = Lists.newArrayList();
            destinations.add(to);
            edges.put(from, destinations);
        }
    }

    public boolean nodeExists(JavaClass javaClass) {
        return nodes.contains(javaClass);
    }

    public JavaClass getNode(JavaClass javaClass) {
        for (JavaClass existingNode : nodes) {
            if (existingNode.equals(javaClass)) {
                return existingNode;
            }
        }
        return null;
    }

    public void addEdge(String from, String to) {
        from = normaliseClassName(from);
        to = normaliseClassName(to);
        JavaClass fromClass = findByClassName(from);
        JavaClass toClass = findByClassName(to);
        if (fromClass != null && toClass != null && !fromClass.equals(toClass)) {
            addEdge(fromClass, toClass);
        } else if (!fromClass.equals(toClass)){
            graph.appendLogging("FAILED DEPENDENCY from " + from + " to " + to, fromClass);
        }

    }

    private String normaliseClassName(String className) {
        if (className.contains(".")) {
            return className.substring(0, className.indexOf("."));
        }
        return className;
    }

    private JavaClass findByClassName(String className) {
        for (JavaClass javaClass : nodes) {
            if (javaClass.className.equals(className)) {
                return javaClass;
            }
        }


        return null;
    }

    private Color defineNodeColor(String className) {
        Color color = Color.BLUE;
        if (StringUtils.endsWith(className, "Resource")) {
            color = Color.RED;
        }
        if (StringUtils.endsWith(className, "Manager")) {
            color = Color.GREEN;
        }

        if (StringUtils.endsWith(className, "Broker")) {
            color = Color.ORANGE;
        }
        return color;
    }
}
