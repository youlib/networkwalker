package uk.ac.open.t802.network.walker.member;

import com.google.common.collect.Sets;
import uk.ac.open.t802.network.walker.domain.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Author youlib
 * Since version 1.0
 */
public class DirectoryWalker {

    private final String JAVA_FILE_EXTENSION = ".java";
    private final String TARGET = "target/classes";
    private final String TEST = "test";
    private final String FEX_WEB_SERVICE = "java/models/flexforce-ws/";
    private final String STANDALONE = "java/standalone/";

    public MemberGraph memberGraph;
    public MemberGraph resourcesMemberGraph;

    public Graph resourcesGraph;
    public Graph graph;

    public DependencyGraph dependencyGraph;
    public DependencyGraph resourcesDependencyGraph;

    public void walkResources(Boolean multigraph) throws IOException {
        resourcesGraph = new Graph();
        resourcesMemberGraph = new MemberGraph(new IdGenerator(), resourcesGraph, multigraph);
        resourcesDependencyGraph = new DependencyGraph(new IdGenerator(), resourcesGraph, multigraph);

        for (String resourceDirectory : Resources.RESOURCE_MAP.keySet()) {
            Path path = Paths.get(resourceDirectory);

            System.out.println("Network walking started for path " + resourceDirectory);
            InterfaceWalker interfaceWalker = new InterfaceWalker(resourcesMemberGraph, resourcesDependencyGraph);
            ResourcesDirectoryVisitor nodeVisitor = new ResourcesDirectoryVisitor(interfaceWalker, resourceDirectory);
            Files.walkFileTree(path, nodeVisitor);

            ClassWalker classWalker = new ClassWalker(resourcesGraph, resourcesMemberGraph, resourcesDependencyGraph);
            ResourcesDirectoryVisitor classVisitor = new ResourcesDirectoryVisitor(classWalker, resourceDirectory);
            Files.walkFileTree(path, classVisitor);
            System.out.println("Network walking done for path  " + resourceDirectory);

        }
    }

    public void walkDirectory(String rootDirectory, boolean walkTestFolders, boolean multigraph) throws IOException {

        Path path = Paths.get(rootDirectory);
        DublicatesHunter dublicatesHunter = new DublicatesHunter();

        Files.walkFileTree(path, dublicatesHunter);

        graph = new Graph();

        memberGraph = new MemberGraph(new IdGenerator(), graph, multigraph);
        dependencyGraph = new DependencyGraph(new IdGenerator(), graph, multigraph);

        InterfaceWalker interfaceWalker = new InterfaceWalker(memberGraph, dependencyGraph);
        DirectoryVisitor nodeVisitor = new DirectoryVisitor(interfaceWalker, walkTestFolders);
        Files.walkFileTree(path, nodeVisitor);
        System.out.println("InterfaceWalker done");

        ClassWalker classWalker = new ClassWalker(graph, memberGraph, dependencyGraph);
        DirectoryVisitor classVisitor = new DirectoryVisitor(classWalker, walkTestFolders);
        Files.walkFileTree(path, classVisitor);
        System.out.println("ClassWalker done");

        MemberWalker memberWalker = new MemberWalker(graph, memberGraph, resourcesMemberGraph);
        DirectoryVisitor memberVisitor = new DirectoryVisitor(memberWalker, walkTestFolders);
        Files.walkFileTree(path, memberVisitor);
        System.out.println("MemberWalker done");

        generateDependencyGraph(memberGraph.getEdges(), dependencyGraph);
        System.out.println("Dependencies done");

    }

    private void generateDependencyGraph(Map<Member, List<Member>> edges, DependencyGraph dependencyGraph) {

        for (Member member : edges.keySet()) {

            for (Member innerMember : edges.get(member)) {

                dependencyGraph.addEdge(member.className, innerMember.className);
            }

        }
    }

    private class DublicatesHunter extends SimpleFileVisitor<Path> {

        private Set<String> existingClasses = Sets.newHashSet();

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

            if (attrs.isRegularFile()
                && file.getFileName().toString().endsWith(JAVA_FILE_EXTENSION)
                && !file.toAbsolutePath().toString().contains(TARGET)
                && !file.toAbsolutePath().toString().contains(FEX_WEB_SERVICE)
                && !file.toAbsolutePath().toString().contains(STANDALONE)
                && !file.toAbsolutePath().toString().contains(TEST)) {
                if (existingClasses.contains(file.getFileName().toString())) {
                    throw new RuntimeException("Duplicate found! " + file.getFileName());
                }
                existingClasses.add(file.getFileName().toString());

            }
            return FileVisitResult.CONTINUE;
        }
    }

    private class ResourcesDirectoryVisitor extends SimpleFileVisitor<Path> {

        private final Walker walker;
        private final String path;

        public ResourcesDirectoryVisitor(Walker walker, String path) {
            this.walker = walker;
            this.path = path;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            if (attrs.isRegularFile()
                && file.getFileName().toString().endsWith(JAVA_FILE_EXTENSION)
                && !file.toAbsolutePath().toString().contains(TARGET)
                && Resources.RESOURCE_MAP.get(path).contains(file.getFileName().toString().substring(0, file.getFileName().toString().indexOf(".")))) {
                walker.walkClass(file);

            }
            return FileVisitResult.CONTINUE;
        }

    }

    private class DirectoryVisitor extends SimpleFileVisitor<Path> {

        private final Walker walker;
        private final boolean walkTestFolders;

        public DirectoryVisitor(Walker walker, boolean walkTestFolders) {
            this.walker = walker;
            this.walkTestFolders = walkTestFolders;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

            if (attrs.isRegularFile()
                && file.getFileName().toString().endsWith(JAVA_FILE_EXTENSION)
                && !file.toAbsolutePath().toString().contains(TARGET)
                && !file.toAbsolutePath().toString().contains(STANDALONE)
                && !file.toAbsolutePath().toString().contains(FEX_WEB_SERVICE)) {

                if (walkTestFolders) {
                    walker.walkClass(file);
                } else if (!file.toAbsolutePath().toString().contains(TEST)) {
                    walker.walkClass(file);

                }
            }
            return FileVisitResult.CONTINUE;
        }

    }

}
