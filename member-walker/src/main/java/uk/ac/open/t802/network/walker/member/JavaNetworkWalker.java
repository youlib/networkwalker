package uk.ac.open.t802.network.walker.member;

import org.joda.time.DateTime;
import java.io.*;

/**
 * @author Georgia Bogdanou
 */
public class JavaNetworkWalker {

    private static final String ROOT_PATH = "/Users/youlib/dev/code/java";
//    private static final String ROOT_PATH = "/Users/youlib/dev/codeCodeAnalysis/java";
//    private static final String STORE_PATH = "graphGraph/";
    private static final String STORE_PATH = "graphMultigraph/";
private static final String  SONAR_FILE = "/Users/youlib/Dropbox/T802/antlr/javaNetworkWalker/sonar/sonarQubeExport.txt";

    public static void main(final String[] args) {

        System.out.println("Process starting at " + new DateTime(System.currentTimeMillis()));
        try {

//            Boolean multigrah = false;
            Boolean multigrah = true;
            DataRepresentationGenerator dataRepresentationGenerator = new DataRepresentationGenerator();
            NetworkSonarQubeComparator comparator = new NetworkSonarQubeComparator();

            DirectoryWalker directoryWalker = new DirectoryWalker();

            directoryWalker.walkResources(multigrah);

            directoryWalker.walkDirectory(ROOT_PATH, false, multigrah);

            FileUtils.exportToFile(STORE_PATH + "/log.txt", directoryWalker.graph.getLogging());
            FileUtils.exportToFile(STORE_PATH + "/failedEdges.txt", directoryWalker.graph.getFailedEdges());
            FileUtils.exportToFile(STORE_PATH + "/variables.txt", dataRepresentationGenerator.toVariables(directoryWalker.graph));

            FileUtils.exportToFile(STORE_PATH + "/memberNodes.txt", dataRepresentationGenerator.toNodes(directoryWalker.memberGraph));
            FileUtils.exportToFile(STORE_PATH + "/memberGraph.dot", dataRepresentationGenerator.toOmniGraph(directoryWalker.memberGraph));
            FileUtils.exportToFile(STORE_PATH + "/memberGraphNumbered.dot", dataRepresentationGenerator.toOmniGraphNumbered(directoryWalker.memberGraph));
            FileUtils.exportToFile(STORE_PATH + "/memberStatistics.txt", dataRepresentationGenerator.toStatistics(directoryWalker.memberGraph));
            FileUtils.exportToFile(STORE_PATH + "/memberFlatResults.txt", dataRepresentationGenerator.toFlatResults(directoryWalker.memberGraph));
            FileUtils.exportToFile(STORE_PATH + "/memberBarChart.txt", dataRepresentationGenerator.toBarChart(directoryWalker.memberGraph));
            FileUtils.exportToFile(STORE_PATH + "/powerNodes5andOver.txt", dataRepresentationGenerator.toPowerNodes(directoryWalker.memberGraph, 5, 100));
            FileUtils.exportToFile(STORE_PATH + "/powerNodes2.txt", dataRepresentationGenerator.toPowerNodes(directoryWalker.memberGraph, 2, 2));
            FileUtils.exportToFile(STORE_PATH + "/powerNodes3.txt", dataRepresentationGenerator.toPowerNodes(directoryWalker.memberGraph, 3, 3));
            FileUtils.exportToFile(STORE_PATH + "/powerNodes4.txt", dataRepresentationGenerator.toPowerNodes(directoryWalker.memberGraph, 4, 4));
            FileUtils.exportToFile(STORE_PATH + "/powerNodes1.txt", dataRepresentationGenerator.toPowerNodes(directoryWalker.memberGraph, 1, 1));
            FileUtils.exportToFile(STORE_PATH + "/memberOrphans.txt", dataRepresentationGenerator.toOrphans(directoryWalker.memberGraph));

            FileUtils.exportToFile(STORE_PATH + "/dependencyNodes.txt", dataRepresentationGenerator.toNodes(directoryWalker.dependencyGraph));
            FileUtils.exportToFile(STORE_PATH + "/dependencyGraph.dot", dataRepresentationGenerator.toOmniGraph(directoryWalker.dependencyGraph));
            FileUtils.exportToFile(STORE_PATH + "/dependencyGraphNumbered.dot", dataRepresentationGenerator.toOmniGraphNumbered(directoryWalker.dependencyGraph));
            FileUtils.exportToFile(STORE_PATH + "/dependencyStatistics.txt", dataRepresentationGenerator.toStatistics(directoryWalker.dependencyGraph));
            FileUtils.exportToFile(STORE_PATH + "/dependencyFlatResults.txt", dataRepresentationGenerator.toFlatResults(directoryWalker.dependencyGraph));
            FileUtils.exportToFile(STORE_PATH + "/dependencyOrphans.txt", dataRepresentationGenerator.toOrphans(directoryWalker.dependencyGraph));
            FileUtils.exportToFile(STORE_PATH + "/dependencyBarChart.txt", dataRepresentationGenerator.toBarChart(directoryWalker.dependencyGraph));

            FileUtils.exportToFile(STORE_PATH + "/resourceMemberNodes.txt", dataRepresentationGenerator.toNodes(directoryWalker.resourcesMemberGraph));
            FileUtils.exportToFile(STORE_PATH + "/resourceVariables.dot", dataRepresentationGenerator.toOmniGraph(directoryWalker.resourcesMemberGraph));


            FileUtils.exportToFile(STORE_PATH + "/memberCytoscapeGraphOrphans.xgmml", dataRepresentationGenerator.toXGMML(directoryWalker.memberGraph));
            FileUtils.exportToFile(STORE_PATH + "/dependencyCytoscapeGraphOrphans.xgmml", dataRepresentationGenerator.toXGMML(directoryWalker.dependencyGraph));

            DegreeCalculatorUtil.colorInDegrees(directoryWalker.memberGraph);
            FileUtils.exportToFile(STORE_PATH + "/memberCytoscapeGraphInDegree.xgmml", dataRepresentationGenerator.toXGMMLWithoutOrphans(directoryWalker.memberGraph));
            DegreeCalculatorUtil.colorOutDegrees(directoryWalker.memberGraph);
            FileUtils.exportToFile(STORE_PATH + "/memberCytoscapeGraphOutDegree.xgmml", dataRepresentationGenerator.toXGMMLWithoutOrphans(directoryWalker.memberGraph));

            DegreeCalculatorUtil.colorInDegrees(directoryWalker.dependencyGraph);
            FileUtils.exportToFile(STORE_PATH + "/dependencyCytoscapeGraphInDegree.xgmml", dataRepresentationGenerator.toXGMMLWithoutOrphans(directoryWalker.dependencyGraph));
            DegreeCalculatorUtil.colorOutDegrees(directoryWalker.dependencyGraph);
            FileUtils.exportToFile(STORE_PATH + "/dependencyCytoscapeGraphOutDegree.xgmml", dataRepresentationGenerator.toXGMMLWithoutOrphans(directoryWalker.dependencyGraph));



            FileUtils.exportToFile(STORE_PATH + "/comparison.txt", comparator.doComparison(directoryWalker.memberGraph, SONAR_FILE));


        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
        System.out.println("Process completed at " + new DateTime(System.currentTimeMillis()));
    }

}
