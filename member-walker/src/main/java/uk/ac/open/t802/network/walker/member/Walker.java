package uk.ac.open.t802.network.walker.member;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import uk.ac.open.t802.network.walker.grammar.JavaLexer;
import uk.ac.open.t802.network.walker.grammar.JavaParser;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;

/**
 * Author youlib
 * Since version 1.0
 */
public interface Walker {

    default ParseTree setupParseTree(Path path) {
        try {
            File file = path.toFile();
            InputStream is = new FileInputStream(file);
            ANTLRInputStream input = new ANTLRInputStream(is);
            JavaLexer lexer = new JavaLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            JavaParser parser = new JavaParser(tokens);
            parser.setBuildParseTree(true);
            return parser.compilationUnit();

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    void walkClass(Path path);
}
