package uk.ac.open.t802.network.walker.member;

import uk.ac.open.t802.network.walker.domain.*;

/**
 * @author Georgia Bogdanou
 */
public class DegreeCalculatorUtil {

    public static void colorOutDegrees(DependencyGraph dependencyGraph) {
        for (JavaClass node : dependencyGraph.nodes) {
            node.color = calculateColor(calculateOutDegree(node, dependencyGraph));
        }
    }

    private static Integer calculateOutDegree(JavaClass node, DependencyGraph dependencyGraph) {
        if (dependencyGraph.edges.get(node) != null) {
            return dependencyGraph.edges.get(node).size();
        }
        return 0;
    }

    public static void colorInDegrees(MemberGraph memberGraph) {
        for (Member member : memberGraph.getNodes()) {
            member.color = calculateColor(calculateInDegree(member, memberGraph));
        }
    }

    private static Color calculateColor(Integer degree) {
        if (degree >= 7) {
            return Color.RED;
        }
        if (degree >= 5) {
            return Color.ORANGE;
        }
        if (degree >= 3) {
            return Color.GREEN;
        }
        return Color.BLUE;
    }

    public static void colorOutDegrees(MemberGraph memberGraph) {
        for (Member member : memberGraph.getNodes()) {
            member.color = calculateColor(calculateOutDegree(member, memberGraph));
        }

    }

    public static void colorInDegrees(DependencyGraph dependencyGraph) {
        for (JavaClass node : dependencyGraph.nodes) {
            node.color = calculateColor(calculateInDegree(node, dependencyGraph));
        }
    }

    private static Integer calculateInDegree(JavaClass javaClass, DependencyGraph dependencyGraph) {
        Integer count = 0;
        for (JavaClass node : dependencyGraph.edges.keySet()) {
            if (dependencyGraph.edges.get(node).contains(javaClass)) {
                for (JavaClass edge : dependencyGraph.edges.get(node)) {
                    if (edge.equals(javaClass)) {
                        count = count + 1;
                    }
                }
            }

        }
        return count;    }

    public static Integer calculateInDegree(Member member, MemberGraph memberGraph) {

        Integer count = 0;

        for (Member node : memberGraph.getEdges().keySet()) {
            if (memberGraph.getEdges().get(node).contains(member)) {
                for (Member edge : memberGraph.getEdges().get(node)) {
                    if (edge.equals(member)) {
                        count = count + 1;
                    }
                }
            }

        }
        return count;
    }

    public static Integer calculateOutDegree(Member member, MemberGraph memberGraph) {
        if (memberGraph.getEdges().get(member) != null) {
            return memberGraph.getEdges().get(member).size();
        }
        return 0;
    }

}
