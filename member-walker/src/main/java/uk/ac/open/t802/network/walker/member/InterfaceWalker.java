package uk.ac.open.t802.network.walker.member;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import uk.ac.open.t802.network.walker.domain.DependencyGraph;
import uk.ac.open.t802.network.walker.domain.MemberGraph;
import uk.ac.open.t802.network.walker.listener.InterfaceWalkerListener;

import java.nio.file.Path;

/**
 * @author Georgia Bogdanou
 */
public class InterfaceWalker implements Walker {

    private final MemberGraph memberGraph;
    private final DependencyGraph dependencyGraph;

    public InterfaceWalker(MemberGraph memberGraph, DependencyGraph dependencyGraph) {
        this.memberGraph = memberGraph;
        this.dependencyGraph = dependencyGraph;
    }

    @Override
    public void walkClass(Path path) {

        String className = path.getFileName().toString().substring(0, path.getFileName().toString().indexOf("."));

        ParseTree parseTree = this.setupParseTree(path);
        ParseTreeWalker walker = new ParseTreeWalker();

        InterfaceWalkerListener intefraceWalkerListener = new InterfaceWalkerListener(dependencyGraph, memberGraph, className);
        walker.walk(intefraceWalkerListener, parseTree);


    }
}
