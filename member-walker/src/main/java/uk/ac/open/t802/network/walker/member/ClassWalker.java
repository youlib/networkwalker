package uk.ac.open.t802.network.walker.member;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import uk.ac.open.t802.network.walker.domain.DependencyGraph;
import uk.ac.open.t802.network.walker.domain.Graph;
import uk.ac.open.t802.network.walker.domain.IdGenerator;
import uk.ac.open.t802.network.walker.domain.MemberGraph;
import uk.ac.open.t802.network.walker.listener.ClassWalkerListener;

import java.nio.file.Path;

/**
 * Author youlib
 * Since version 1.0
 */
public class ClassWalker implements Walker {

    private final Graph graph;
    private final MemberGraph memberGraph;
    private final DependencyGraph dependencyGraph;

    public ClassWalker(Graph graph, MemberGraph memberGraph, DependencyGraph dependencyGraph) {
        this.graph = graph;
        this.memberGraph = memberGraph;
        this.dependencyGraph = dependencyGraph;
    }

    @Override
    public void walkClass(Path path) {
        String className = path.getFileName().toString().substring(0, path.getFileName().toString().indexOf("."));

        ParseTree parseTree = this.setupParseTree(path);
        ParseTreeWalker walker = new ParseTreeWalker();

        ClassWalkerListener classWalkerListener = new ClassWalkerListener(graph, memberGraph, dependencyGraph, className);
        walker.walk(classWalkerListener, parseTree);
    }
}
