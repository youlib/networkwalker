package uk.ac.open.t802.network.walker.member;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import uk.ac.open.t802.network.walker.domain.*;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;

/**
 * Author youlib
 * Since version 1.0
 */
public class DataRepresentationGenerator {

    public String toBarChart(DependencyGraph graph) {
        SortedMap<Integer, Integer> results = Maps.newTreeMap();

        for (JavaClass node : graph.nodes) {
            Integer noOfEdges = graph.edges.getOrDefault(node, Lists.newArrayList()).size();
            addToResults(results, noOfEdges);

        }

        return produceBarChart(results);
    }

    public String toBarChart(MemberGraph graph) {
        Map<Member, Set<Member>> reversedMap = reverseMap(graph);

        SortedMap<Integer, Integer> results = Maps.newTreeMap();

        for (Member node : graph.getNodes()) {
            Integer noOfEdges = reversedMap.getOrDefault(node, Sets.newHashSet()).size();
            addToResults(results, noOfEdges);

        }
        return produceBarChart(results);

    }

    private String produceBarChart(SortedMap<Integer, Integer> results) {
        StringBuilder buf = new StringBuilder();

        for (Integer edgeCount : results.keySet()) {
            buf.append(edgeCount);
            buf.append("\t");
            buf.append(results.get(edgeCount));
            buf.append("\n");
        }

        return buf.toString();
    }

    public String toOrphans(DependencyGraph graph) {
        StringBuilder buf = new StringBuilder();
        Set<JavaClass> outDegreeZero = Sets.newHashSet();
        Set<JavaClass> orphans = Sets.newTreeSet();
        for (JavaClass node : graph.nodes) {
            if (!graph.edges.containsKey(node)) {
                outDegreeZero.add(node);
            }

        }
        for (JavaClass node : outDegreeZero) {
            boolean hasEdge = false;
            for (JavaClass key : graph.edges.keySet()) {
                if (graph.edges.get(key).contains(node)) {
                    hasEdge = true;
                }
                if (hasEdge) {
                    break;
                }
            }
            if (!hasEdge) {
                orphans.add(node);
            }

        }

        for (JavaClass node : orphans) {
            buf.append(node);
            buf.append("\n");
        }
        return buf.toString();

    }

    public String toFlatResults(DependencyGraph graph) {
        StringBuilder buf = new StringBuilder();
        for (JavaClass node : graph.nodes) {
            if (graph.edges.containsKey(node) && graph.edges.get(node).size() > 0) {
                buf.append(node);
                buf.append("\t");
                buf.append("- >");
                buf.append("\t");
                buf.append(graph.edges.get(node));
                buf.append("\n");

            }
        }

        return buf.toString();
    }

    public String toFlatResults(MemberGraph graph) {
        StringBuilder buf = new StringBuilder();
        for (Member node : graph.getNodes()) {
            if (graph.getEdges().containsKey(node) && graph.getEdges().get(node).size() > 0) {
                buf.append(node);
                buf.append("\t");
                buf.append("- >");
                buf.append("\t");
                buf.append(graph.getEdges().get(node));
                buf.append("\n");

            }
        }

        return buf.toString();
    }

    private void addToResults(SortedMap<Integer, Integer> results, Integer noOfEdges) {

        if (results.containsKey(noOfEdges)) {
            Integer existingNodes = results.get(noOfEdges);
            results.put(noOfEdges, existingNodes + 1);
        } else {
            results.put(noOfEdges, 1);

        }

    }

    public String toOmniGraph(DependencyGraph graph) {
        StringBuilder buf = new StringBuilder();
        buf.append("digraph G {\n");
        buf.append("  ranksep=.25;\n");
        buf.append("  edge [arrowsize=.5]\n");
        buf.append("  node [shape=circle, fontname=\"ArialNarrow\",\n");
        buf.append("        fontsize=12, fixedsize=true, height=.45];\n");
        buf.append("  ");
        for (JavaClass src : graph.edges.keySet()) {
            for (JavaClass trg : graph.edges.get(src)) {
                buf.append("\"");
                buf.append(src.packageName);
                buf.append(".");
                buf.append(src.className);
                buf.append("\" -> \"");
                buf.append(trg.packageName);
                buf.append(".");
                buf.append(trg.className);
                buf.append("\";\n");
            }
        }
        buf.append("}\n");

        return buf.toString();

    }

    public String toOmniGraph(MemberGraph graph) {
        StringBuilder buf = new StringBuilder();
        buf.append("digraph G {\n");
        buf.append("  ranksep=.25;\n");
        buf.append("  edge [arrowsize=.5]\n");
        buf.append("  node [shape=circle, fontname=\"ArialNarrow\",\n");
        buf.append("        fontsize=12, fixedsize=true, height=.45];\n");
        buf.append("  ");
        for (Member src : graph.getEdges().keySet()) {
            for (Member trg : graph.getEdges().get(src)) {
                buf.append("\"");
                buf.append(src.className);
                buf.append(".");
                buf.append(src.methodName);
                buf.append(src.methodArguments);
                buf.append("\" -> \"");
                buf.append(trg.className);
                buf.append(".");
                buf.append(trg.methodName);
                buf.append(trg.methodArguments);
                buf.append("\";\n");
            }
        }
        buf.append("}\n");

        return buf.toString();

    }

    public String toStatistics(DependencyGraph graph) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Number of nodes ");
        stringBuilder.append(graph.nodes.size());
        stringBuilder.append("\n");
        stringBuilder.append("Number of edges ");
        int count = 0;
        for (JavaClass src : graph.edges.keySet()) {
            count = count + graph.edges.get(src).size();
        }
        stringBuilder.append(count);
        stringBuilder.append("\n");

        return stringBuilder.toString();
    }

    public String toXGMML(MemberGraph graph) {
        Set<Member> nodes = graph.getNodes();

        StringBuilder buf = new StringBuilder();
        printXGMMLHeaders(buf);
        printMemberNodes(buf, nodes);
        printMemberEdges(buf, graph.getEdges());
        buf.append("</graph>");
        return buf.toString();
    }

    private void printMemberEdges(StringBuilder buf, Map<Member, List<Member>> edges) {
        for (Member src : edges.keySet()) {
            for (Member trg : edges.get(src)) {
                buf.append("<edge source=\"");
                buf.append(src.id);
                buf.append("\" target=\"");
                buf.append(trg.id);
                buf.append("\" >");
                buf.append("\n");
                buf.append("</edge>");
                buf.append("\n");
            }
        }
    }

    public String toXGMML(DependencyGraph graph) {
        Set<JavaClass> nodes = graph.nodes;

        StringBuilder buf = new StringBuilder();
        printXGMMLHeaders(buf);
        printJavaClassNodes(buf, nodes);
        printJavaClassEdges(buf, graph.edges);
        buf.append("</graph>");
        return buf.toString();
    }

    private Map<Member, Set<Member>> reverseMap(MemberGraph graph) {
        Map<Member, Set<Member>> reversedEdges = Maps.newHashMap();

        for (Member member : graph.getEdges().keySet()) {
            for (Member target : graph.getEdges().get(member)) {
                if (reversedEdges.containsKey(target)) {
                    reversedEdges.get(target).add(member);
                } else {
                    Set<Member> destinations = Sets.newLinkedHashSet();
                    destinations.add(member);
                    reversedEdges.put(target, destinations);
                }

            }
        }
        return reversedEdges;
    }

    public String toPowerNodes(MemberGraph graph, int min, int max) {
        Map<Member, Set<Member>> reversedMap = reverseMap(graph);

        StringBuilder stringBuilder = new StringBuilder();

        for (Member member : reversedMap.keySet()) {
            if (reversedMap.get(member).size() >= min && reversedMap.get(member).size() <= max) {
                stringBuilder.append(reversedMap.get(member).size());
                stringBuilder.append(":");
                stringBuilder.append("\t");
                stringBuilder.append(member);
                stringBuilder.append("\t");
                stringBuilder.append("- >");
                stringBuilder.append("\t");
                stringBuilder.append(reversedMap.get(member));
                stringBuilder.append("\n");

            }
        }
        return stringBuilder.toString();

    }

    public String toStatistics(MemberGraph graph) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Number of nodes ");
        stringBuilder.append(graph.getNodes().size());
        stringBuilder.append("\n");
        stringBuilder.append("Number of edges ");
        int count = 0;
        for (Member src : graph.getEdges().keySet()) {
            count = count + graph.getEdges().get(src).size();
        }
        stringBuilder.append(count);
        stringBuilder.append("\n");

        return stringBuilder.toString();
    }

    public String toOmniGraphNumbered(DependencyGraph graph) {
        StringBuilder buf = new StringBuilder();
        buf.append("digraph G {\n");
        buf.append("  ranksep=.25;\n");
        buf.append("  edge [arrowsize=.5]\n");
        buf.append("  node [shape=circle, fontname=\"ArialNarrow\",\n");
        buf.append("        fontsize=12, fixedsize=true, height=.45];\n");
        buf.append("  ");
        for (JavaClass node : graph.nodes) { // print all nodes first
            if (graph.edges.keySet().contains(node)) {
                buf.append(node.id);
                buf.append(" [fillcolor = " + node.color.getRgbValue() + "]");
                buf.append("; ");
            }
        }
        buf.append("\n");
        for (JavaClass src : graph.edges.keySet()) {
            for (JavaClass trg : graph.edges.get(src)) {
                buf.append(" ");
                buf.append(src.id);
                buf.append(" -> ");
                buf.append(trg.id);
                buf.append(";\n");
            }
        }
        buf.append("}\n");

        return buf.toString();

    }

    public String toOmniGraphNumbered(MemberGraph graph) {
        StringBuilder buf = new StringBuilder();
        buf.append("digraph G {\n");
        buf.append("  ranksep=.25;\n");
        buf.append("  edge [arrowsize=.5]\n");
        buf.append("  node [shape=circle, fontname=\"ArialNarrow\",\n");
        buf.append("        fontsize=12, fixedsize=true, height=.45];\n");
        buf.append("  ");
        for (Member node : graph.getNodes()) { // print all nodes first
            if (graph.getEdges().keySet().contains(node)) {
                buf.append(node.id);
                buf.append(" [fillcolor = " + node.color.getRgbValue() + "]");
                buf.append("; ");
            }
        }
        buf.append("\n");
        for (Member src : graph.getEdges().keySet()) {
            for (Member trg : graph.getEdges().get(src)) {
                buf.append(" ");
                buf.append(src.id);
                buf.append(" -> ");
                buf.append(trg.id);
                buf.append(";\n");
            }
        }
        buf.append("}\n");

        return buf.toString();

    }

    public String toNodes(MemberGraph graph) {
        StringBuilder buf = new StringBuilder();

        graph.getNodes().forEach(node -> buf.append(node + "\n"));

        return buf.toString();
    }

    public String toNodes(DependencyGraph graph) {
        StringBuilder buf = new StringBuilder();

        graph.nodes.forEach(node -> buf.append(node + "\n"));

        return buf.toString();
    }

    public String toVariables(Graph graph) {
        StringBuilder buf = new StringBuilder();

        for (String className : graph.variableMaps.keySet()) {

            for (String variable : graph.variableMaps.get(className).keySet()) {
                buf.append(" ");
                buf.append(className);
                buf.append(" -> ");
                buf.append(variable);
                buf.append(" -> ");
                buf.append(graph.variableMaps.get(className).get(variable));
                buf.append(";\n");
            }
            buf.append("}\n");

        }
        return buf.toString();
    }

    public String toOrphans(MemberGraph graph) {
        StringBuilder buf = new StringBuilder();
        Set<Member> outDegreeZero = Sets.newHashSet();
        Set<Member> orphans = Sets.newTreeSet();
        for (Member node : graph.getNodes()) {
            if (!graph.getEdges().containsKey(node)) {
                outDegreeZero.add(node);
            }

        }
        for (Member node : outDegreeZero) {
            boolean hasEdge = false;
            for (Member key : graph.getEdges().keySet()) {
                if (graph.getEdges().get(key).contains(node)) {
                    hasEdge = true;
                }
                if (hasEdge) {
                    break;
                }
            }
            if (!hasEdge) {
                orphans.add(node);
            }

        }

        for (Member node : orphans) {
            buf.append(node);
            buf.append("\n");
        }
        return buf.toString();
    }

    public String toXGMMLWithoutOrphans(MemberGraph graph) {
        Set<Member> nodes = Sets.newHashSet();
        for (Member member : graph.getEdges().keySet()) {
            nodes.add(member);
            nodes.addAll(graph.getEdges().get(member));
        }

        StringBuilder buf = new StringBuilder();
        printXGMMLHeaders(buf);
        printMemberNodes(buf, nodes);
        printMemberEdges(buf, graph.getEdges());
        buf.append("</graph>");
        return buf.toString();
    }

    private void printMemberNodes(StringBuilder buf, Set<Member> nodes) {
        for (Member node : nodes) { // print all nodes first
            printNode(buf, node.id, node.color.getRgbValue());
        }
    }

    private void printNode(StringBuilder buf, int id, String color) {
        buf.append("<node id=\"");
        buf.append(id);
        buf.append("\" label=\"");
        buf.append(id);
        buf.append("\" >");
        buf.append("\n");
//            buf.append("<graphics type=\"circle\" x=\"360\" y=\"360\">\n </graphics>");
        buf.append("<graphics type=\"circle\"");
        buf.append(" fill=\"");
        buf.append(color);
        buf.append("\"> </graphics>");
        buf.append("\n");
        buf.append("</node>");
        buf.append("\n");
    }

    public String toXGMMLWithoutOrphans(DependencyGraph graph) {
        Set<JavaClass> nodes = Sets.newHashSet();
        for (JavaClass javaClass : graph.edges.keySet()) {
            nodes.add(javaClass);
            nodes.addAll(graph.edges.get(javaClass));
        }

        StringBuilder buf = new StringBuilder();
        printXGMMLHeaders(buf);
        printJavaClassNodes(buf, nodes);
        printJavaClassEdges(buf, graph.edges);

        buf.append("</graph>");
        return buf.toString();
    }

    private void printXGMMLHeaders(StringBuilder buf){
        buf.append("<?xml version='1.0'?>");
        buf.append("\n");
        buf.append("<!DOCTYPE graph SYSTEM \"http://www.cs.rpi.edu/~puninj/XGMML/xgmml.dtd\">");
        buf.append("\n");
        buf.append("<graph directed=\"1\" Layout=\"points\">");
        buf.append("\n");
    }


    private void printJavaClassEdges(StringBuilder buf, Map<JavaClass, List<JavaClass>> edges) {
        for (JavaClass src : edges.keySet()) {
            for (JavaClass trg : edges.get(src)) {
                printEdge(buf, src.id, trg.id);
            }
        }
    }

    private void printEdge(StringBuilder buf, int sourceId, int targetId) {
        buf.append("<edge source=\"");
        buf.append(sourceId);
        buf.append("\" target=\"");
        buf.append(targetId);
        buf.append("\" >");
        buf.append("\n");
        buf.append("</edge>");
        buf.append("\n");
    }

    private void printJavaClassNodes(StringBuilder buf, Set<JavaClass> nodes) {
        for (JavaClass node : nodes) { // print all nodes first
            printNode(buf, node.id, node.color.getRgbValue());
        }
    }
}
