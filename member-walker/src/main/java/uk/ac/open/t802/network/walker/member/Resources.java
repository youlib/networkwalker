package uk.ac.open.t802.network.walker.member;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.List;
import java.util.Map;

/**
 * @author Georgia Bogdanou
 */
public class Resources {

    public static final Map<String, List<String>> RESOURCE_MAP = Maps.newHashMap();

   static  {
        RESOURCE_MAP.put("/Users/youlib/Java/java/lang", Lists.newArrayList("Object", "Class", "System", "Long", "Integer", "Enum", "Float", "Double", "Boolean", "Character"
            , "String", "StringBuilder", "Math"));
        RESOURCE_MAP.put("/Users/youlib/Java/java/util", Lists.newArrayList("List", "Map", "Set", "HashSet", "HashMap", "Logger", "Date", "AtomicInteger", "Calendar", "Collectors"));
        RESOURCE_MAP.put("/Users/youlib/Java/java/nio", Lists.newArrayList("Charset"));
        RESOURCE_MAP.put("/Users/youlib/Java/java/text", Lists.newArrayList("DateFormat"));
        RESOURCE_MAP.put("/Users/youlib/Java/java/math", Lists.newArrayList("BigDecimal"));
        RESOURCE_MAP.put("/Users/youlib/Java/gson/gson/src/main/java/com/google/gson", Lists.newArrayList("JsonElement", "JsonObject", "JsonArray", "JsonParser"));
        RESOURCE_MAP.put("/Users/youlib/Java/guava/guava/src/com/google/common/collect", Lists.newArrayList("Lists", "Sets", "Maps", "BiMap"));
    }
}
