package uk.ac.open.t802.network.walker.member;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import uk.ac.open.t802.network.walker.domain.Member;
import uk.ac.open.t802.network.walker.domain.MemberGraph;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.List;

/**
 * @author Georgia Bogdanou
 */
public class NetworkSonarQubeComparator {

    public String doComparison(MemberGraph memberGraph, String sonarExport) {
        StringBuilder buf = new StringBuilder();
        buf.append("CC");
        buf.append(";");
        buf.append("IN");
        buf.append(";");
        buf.append("OUT");
        buf.append("\n");

        try (BufferedReader br = new BufferedReader(new FileReader(sonarExport))) {
            String line;
            while ((line = br.readLine()) != null) {

                List<String> lineFragments = Lists.newArrayList(line.split(";"));
                String methodName = StringUtils.trim(StringUtils.remove(lineFragments.get(0), ";"));
                Integer cc = Integer.valueOf(StringUtils.trim(StringUtils.remove(lineFragments.get(1), ";")));
                String fullPath = lineFragments.get(2);
                String className = fullPath.substring(fullPath.lastIndexOf("/") + 1, fullPath.lastIndexOf("."));
                Member member = getMember(methodName, className, memberGraph);
                if (member != null) {
                    Integer indegree = DegreeCalculatorUtil.calculateInDegree(member, memberGraph);
                    Integer outdegree = DegreeCalculatorUtil.calculateOutDegree(member, memberGraph);
                    if (indegree > 0 || outdegree > 0) {
                        buf.append(cc);
                        buf.append(";");
                        buf.append(indegree);
                        buf.append(";");
                        buf.append(outdegree);
                        buf.append("\n");

                    }
                }

            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return buf.toString();
    }



    private Member getMember(String methodName, String className, MemberGraph memberGraph) {
        for (Member member : memberGraph.getNodes()) {
            if (member.className.equals(className) && member.methodName.equals(methodName)) {
                return member;
            }
        }
        return null;
    }

}
