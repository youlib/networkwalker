package uk.ac.open.t802.network.walker.member;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import uk.ac.open.t802.network.walker.domain.Graph;
import uk.ac.open.t802.network.walker.domain.MemberGraph;
import uk.ac.open.t802.network.walker.listener.ClassMemberWalkerListener;
import uk.ac.open.t802.network.walker.listener.InterfaceMemberWalkerListener;
import uk.ac.open.t802.network.walker.listener.InterfaceWalkerListener;

import java.nio.file.Path;

/**
 * Author youlib
 * Since version 1.0
 */
public class MemberWalker implements Walker {

    private final Graph graph;
    private final MemberGraph memberGraph;
    private final MemberGraph resourcesGraph;

    public MemberWalker(Graph graph, MemberGraph memberGraph, MemberGraph resourcesGraph) {
        this.graph = graph;
        this.memberGraph = memberGraph;
        this.resourcesGraph = resourcesGraph;
    }

    @Override
    public void walkClass(Path path) {

        String className = path.getFileName().toString().substring(0, path.getFileName().toString().indexOf("."));

        ParseTree parseTree = this.setupParseTree(path);
        ParseTreeWalker walker = new ParseTreeWalker();

        InterfaceMemberWalkerListener  interfaceMemberWalkerListener = new InterfaceMemberWalkerListener(graph, memberGraph, resourcesGraph, className);
        walker.walk(interfaceMemberWalkerListener, parseTree);

        ClassMemberWalkerListener classMemberWalkerListener = new ClassMemberWalkerListener(graph, memberGraph, resourcesGraph, className);
        walker.walk(classMemberWalkerListener, parseTree);

    }

}