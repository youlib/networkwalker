package uk.ac.open.t802.network.walker.domain;

import com.google.common.collect.Lists;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Georgia Bogdanou
 */
public class MemberGraphTest {
    private Graph graph;
    private MemberGraph memberGraph;

    @Before
    public void setup() {
        graph = new Graph();
        memberGraph = new MemberGraph(new IdGenerator(), graph, false);
    }

    @Test
    public void testGraphFindsNode() {

        Member node1 = new Member.Builder(Color.ORANGE).withClassName("SmallClass").withReturnType("void").withMethodName("doSomething").withMethodArguments(Lists.newArrayList("String")).build();
        Member node2 = new Member.Builder(Color.BLUE).withClassName("SmallClass").withReturnType("int").withMethodName("doSomethingElse").withMethodArguments(Lists.newArrayList("String")).build();
        Member node3 = new Member.Builder(Color.RED).withClassName("SmallClass").withReturnType("String").withMethodName("doSomethingThird").withMethodArguments(Lists.newArrayList()).build();

        memberGraph.addNode(node1);
        memberGraph.addNode(node2);
        memberGraph.addNode(node3);

        Assert.assertTrue(memberGraph.getNodes().contains(node1));
        Assert.assertTrue(memberGraph.getNodes().contains(node2));
        Assert.assertTrue(memberGraph.getNodes().contains(node3));

        Assert.assertEquals(memberGraph.getNode(node1).className, node1.className);
        Assert.assertEquals(memberGraph.getNode(node1).returnType, node1.returnType);
        Assert.assertEquals(memberGraph.getNode(node1).methodName, node1.methodName);
        Assert.assertEquals(memberGraph.getNode(node1).methodArguments, node1.methodArguments);
        Assert.assertEquals(memberGraph.getNode(node1).color, node1.color);
        Assert.assertTrue(memberGraph.getNode(node1).id == 1);

        Assert.assertEquals(memberGraph.getNode(node2).className, node2.className);
        Assert.assertEquals(memberGraph.getNode(node2).returnType, node2.returnType);
        Assert.assertEquals(memberGraph.getNode(node2).methodName, node2.methodName);
        Assert.assertEquals(memberGraph.getNode(node2).methodArguments, node2.methodArguments);
        Assert.assertEquals(memberGraph.getNode(node2).color, node2.color);
        Assert.assertTrue(memberGraph.getNode(node2).id == 2);

        Assert.assertEquals(memberGraph.getNode(node3).className, node3.className);
        Assert.assertEquals(memberGraph.getNode(node3).returnType, node3.returnType);
        Assert.assertEquals(memberGraph.getNode(node3).methodName, node3.methodName);
        Assert.assertEquals(memberGraph.getNode(node3).methodArguments, node3.methodArguments);
        Assert.assertEquals(memberGraph.getNode(node3).color, node3.color);
        Assert.assertTrue(memberGraph.getNode(node3).id == 3);

        memberGraph.addEdge(node1, node2);
        memberGraph.addEdge(node1, node3);

        Assert.assertTrue(memberGraph.getEdges().containsKey(node1));
        Assert.assertTrue(memberGraph.getEdges().get(node1).size() == 2);
        Assert.assertTrue(memberGraph.getEdges().get(node1).contains(node2));
        Assert.assertTrue(memberGraph.getEdges().get(node1).contains(node3));
    }

    @Test
    public void testGraphFindsInterfaceOneLevel() {
        String classNode = "Bob";
        String interfaceNode1 = "Interface1";
        String interfaceNode2 = "Interface2";

        Member node1 = new Member.Builder(Color.ORANGE).withClassName(classNode).withReturnType("void").withMethodName("doSomething").withMethodArguments(Lists.newArrayList("String")).build();
        Member node2 = new Member.Builder(Color.BLUE).withClassName(interfaceNode1).withReturnType("void").withMethodName("doSomething").withMethodArguments(Lists.newArrayList("String")).build();
        Member node3 = new Member.Builder(Color.RED).withClassName(interfaceNode2).withReturnType("String").withMethodName("doSomethingThird").withMethodArguments(Lists.newArrayList()).build();

        memberGraph.addNode(node1);
        memberGraph.addNode(node2);
        memberGraph.addNode(node3);

        graph.addInterface(classNode, interfaceNode1);
        graph.addInterface(classNode, interfaceNode2);

        Member node = memberGraph.findNode(node1, true);

        Assert.assertTrue(node.className.equals(interfaceNode1));
    }

    @Test
    public void testGraphFindsInterfaceTwoLevels() {
        String classNode = "Bob";
        String interfaceNode1 = "Interface1";
        String interfaceNode2 = "Interface2";

        Member node1 = new Member.Builder(Color.ORANGE).withClassName(classNode).withReturnType("void").withMethodName("doSomething").withMethodArguments(Lists.newArrayList("String")).build();
        Member node2 = new Member.Builder(Color.BLUE).withClassName(interfaceNode1).withReturnType("String").withMethodName("doSomethingThird").withMethodArguments(Lists.newArrayList("String")).build();
        Member node3 = new Member.Builder(Color.RED).withClassName(interfaceNode2).withReturnType("void").withMethodName("doSomething").withMethodArguments(Lists.newArrayList("String")).build();

        memberGraph.addNode(node1);
        memberGraph.addNode(node2);
        memberGraph.addNode(node3);

        graph.addInterface(classNode, interfaceNode1);
        graph.addInterface(classNode, interfaceNode2);

        Member node = memberGraph.findNode(node1, true);

        Assert.assertTrue(node.className.equals(interfaceNode2));

    }

    @Test
    public void testGraphFindsMatchNode() {
        String classNode = "Bob";

        Member node = new Member.Builder(Color.ORANGE).withClassName(classNode).withReturnType("void").withMethodName("doSomething").withMethodArguments(Lists.newArrayList("String")).build();
        memberGraph.addNode(node);

        Member inputNode = new Member.Builder(Color.BLUE).withClassName(classNode).withMethodName("doSomething").withMethodArguments(Lists.newArrayList("String")).template();

        Member returnedNode = memberGraph.findNode(inputNode, true);

        Assert.assertTrue(node.className.equals(returnedNode.className));
        Assert.assertTrue(node.methodArguments.equals(returnedNode.methodArguments));
        Assert.assertTrue(node.methodName.equals(returnedNode.methodName));
    }

    @Test
    public void testGraphFindsMatchOverloadedNode() {
        String classNode = "Bob";

        Member node1 = new Member.Builder(Color.ORANGE).withClassName(classNode).withReturnType("String").withMethodName("doSomething").withMethodArguments(Lists.newArrayList("String")).build();
        Member node2 = new Member.Builder(Color.ORANGE).withClassName(classNode).withReturnType("String").withMethodName("doSomething").withMethodArguments(Lists.newArrayList("Boolean")).build();
        memberGraph.addNode(node1);
        memberGraph.addNode(node2);

        Member inputNode = new Member.Builder(Color.BLUE).withClassName(classNode).withMethodName("doSomething").withMethodArguments(Lists.newArrayList("Boolean")).template();

        Member returnedNode = memberGraph.findNode(inputNode, true);

        Assert.assertTrue(node2.className.equals(returnedNode.className));
        Assert.assertTrue(node2.methodArguments.equals(returnedNode.methodArguments));
        Assert.assertTrue(node2.methodName.equals(returnedNode.methodName));
    }

    @Test
    public void testGraphMatchesArgumentsInNode() {
        String classNode = "Bob";

        Member node = new Member.Builder(Color.ORANGE).withClassName(classNode).withReturnType("void").withMethodName("doSomething").withMethodArguments(Lists.newArrayList("List<String>")).build();
        memberGraph.addNode(node);

        Member inputNode = new Member.Builder(Color.BLUE).withClassName(classNode).withMethodName("doSomething").withMethodArguments(Lists.newArrayList("List<?>")).template();

        Member returnedNode = memberGraph.findNode(inputNode, true);

        Assert.assertTrue(node.className.equals(returnedNode.className));
        Assert.assertTrue(node.methodArguments.equals(returnedNode.methodArguments));
        Assert.assertTrue(node.methodName.equals(returnedNode.methodName));
    }

    @Test
    public void testGraphFindsGenericNode() {
        String genericClass = "GenericStuffer";

        Member node = new Member.Builder(Color.BLUE).withClassName(genericClass).withReturnType("String").withMethodName("doSomethingThird").withMethodArguments(Lists.newArrayList("?")).build();

        memberGraph.addNode(node);

        Member inputNode = new Member.Builder(Color.BLUE).withClassName(genericClass).withReturnType("String").withMethodName("doSomethingThird").withMethodArguments(Lists.newArrayList("String")).template();

        Member returnedNode = memberGraph.findNode(inputNode, true);

        Assert.assertTrue(node.className.equals(returnedNode.className));
        Assert.assertTrue(node.methodArguments.equals(returnedNode.methodArguments));
        Assert.assertTrue(node.methodName.equals(returnedNode.methodName));

    }

    @Test
    public void testGraphFindsNonGenericNode() {
        String genericClass = "GenericStuffer";

        Member node1 = new Member.Builder(Color.BLUE).withClassName(genericClass).withReturnType("String").withMethodName("doSomethingThird").withMethodArguments(Lists.newArrayList("?")).build();
        Member node2 = new Member.Builder(Color.BLUE).withClassName(genericClass).withReturnType("String").withMethodName("doSomethingThird").withMethodArguments(Lists.newArrayList("String")).build();

        memberGraph.addNode(node1);
        memberGraph.addNode(node2);

        Member inputNode = new Member.Builder(Color.BLUE).withClassName(genericClass).withReturnType("String").withMethodName("doSomethingThird").withMethodArguments(Lists.newArrayList("String")).template();

        Member returnedNode = memberGraph.findNode(inputNode, true);

        Assert.assertTrue(node2.className.equals(returnedNode.className));
        Assert.assertTrue(node2.methodArguments.equals(returnedNode.methodArguments));
        Assert.assertTrue(node2.methodName.equals(returnedNode.methodName));
        Assert.assertTrue(node2.returnType.equals(returnedNode.returnType));

    }




}
