package uk.ac.open.t802.network.walker.member.fixture;


/**
 * Author youlib
 * Since version 1.0
 */
public interface FixtureServiceInterface extends YetAnotherInterface {


    default void someBeans() {
        makeCoffee();
        System.out.println("lalala");
        makeTea();
    }

    void doSomething(String somethingElse, int count, String bob);

    String getMeDelivery(String food);
}
