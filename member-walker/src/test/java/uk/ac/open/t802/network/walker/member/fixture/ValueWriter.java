package uk.ac.open.t802.network.walker.member.fixture;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Georgia Bogdanou
 */
public interface ValueWriter<ValueType> {

    List<ValueType> write(List<BigDecimal> value);

}