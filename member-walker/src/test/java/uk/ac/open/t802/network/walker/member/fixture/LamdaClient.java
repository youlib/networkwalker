package uk.ac.open.t802.network.walker.member.fixture;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * @author Georgia Bogdanou
 */
public class LamdaClient {


    private FixtureService fixtureService = new FixtureService();

    private List<String> listOfStuff = Lists.newArrayList("donkey", "cat", "dog", "mouse", "elephant");

    public void lamdaSomehting(){

        listOfStuff.stream().forEach(stuff -> fixtureService.getMeDelivery(stuff));

        listOfStuff.forEach(stuff -> {
            System.out.print("dah");
            fixtureService.doSomethingElse(String.format("here is some %s", stuff));
        });
    }


    public class MyInnerMagicWorld{

        public String magic(){
            return "magic!";
        }

    }

    public LamdaClient byName(String name) {

        return this;
    }

    public LamdaClient byHeight(int i) {
        return this;
    }

    public Person get() {
        return new Person();
    }
}
