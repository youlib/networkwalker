package uk.ac.open.t802.network.walker.member.fixture;

import com.google.common.collect.Lists;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author youlib
 * Since version 1.0
 */
public class FixtureService extends AbstractService implements FixtureServiceInterface, LalaInterface {

    protected final Map<String, List> samplesByName = new HashMap<>();



    public void useRerson(){

        Person person = new Person.PersonBuilder("james").withAge(8).withLastName("Jameson").build();

        person.evaluate(new ConcreteWriter());
        person.addStuff(Lists.newArrayList());
    }


    @Override
    public void doSomething(String somethingElse, int count, String bob) {
        System.out.println(somethingElse);
        donald.useSomething();
        doNothing();
    }

    private void doNothing() {

    }

    @Override
    public String getMeDelivery(String food) {
        samplesByName.get(food);
        return food;
    }

    public static String doSomethingElse(String something) {
        LamdaClient lamdaClient = new LamdaClient();
        LamdaClient.MyInnerMagicWorld magicWorld = lamdaClient.new  MyInnerMagicWorld();
        String result = magicWorld.magic();
        return result;
    }

    public <ValueType> List<ValueType> evaluate(FixtureWriter<ValueType> sampleWriter, LamdaClient lamdaClient) {

        List<ValueType> result = sampleWriter.doPotatoes(sampleWriter, this);


        Person person = lamdaClient
            .byName("John")
            .byHeight(124)
            .get();

        return result;
    }


    public class ConcreteWriter implements ValueWriter<Integer> {

        @Override
        public List<Integer> write(List<BigDecimal> value) {
            return null;
        }
    }
}
