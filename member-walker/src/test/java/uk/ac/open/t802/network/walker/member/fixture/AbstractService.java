package uk.ac.open.t802.network.walker.member.fixture;

import com.google.common.collect.Lists;
import java.util.List;

/**
 * Author youlib
 * Since version 1.0
 */
public abstract class AbstractService implements YetAnotherInterface {


    protected FixtureClient donald;
    private List<String> myItems = Lists.newArrayList();

    @Override
    public void makeCoffee() {
        myItems.add("coffee");
        System.out.println("br, br,br, boiling coffee");
    }
}
