package uk.ac.open.t802.network.walker.member.fixture;

/**
 * Author youlib
 * Since version 1.0
 */
public interface YetAnotherInterface {

    void makeCoffee();

    default void makeTea() {
    }
}
