package uk.ac.open.t802.network.walker.member;

import com.google.common.collect.Maps;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import uk.ac.open.t802.network.walker.domain.Color;
import uk.ac.open.t802.network.walker.domain.JavaClass;
import java.util.List;
import java.util.Map;

/**
 * Author youlib
 * Since version 1.0
 */
@Ignore
public class DirectoryWalkerDependencyTest {

    private static final String STORE_PATH = "results";
    private static final String ROOT_PATH = "member-walker/src/test/java/uk/ac/open/t802/network/walker/member/fixture";
//    private static final String ROOT_PATH = "member-walker/src/test/java/uk/ac/open/t802/network/walker/member/fixture/subset";
//    private static final String ROOT_PATH = "member-walker/src/test/java/uk/ac/open/t802/network/walker/member/fixture/subset/supersubset";

    private JavaClass node1;
    private JavaClass node2;
    private JavaClass node3;
    private JavaClass node4;
    private JavaClass node5;
    private JavaClass node6;
    private JavaClass node7;
    private JavaClass node8;
    private JavaClass node9;
    private JavaClass node10;

    @Before
    public void setUp() {

        node1 = new JavaClass("uk.ac.open.t802.network.walker.member.fixture", "FixtureServiceInterface", Color.BLUE);
        node2 = new JavaClass("uk.ac.open.t802.network.walker.member.fixture", "FixtureWriter", Color.BLUE);
        node3 = new JavaClass("uk.ac.open.t802.network.walker.member.fixture", "LalaInterface", Color.BLUE);
        node4 = new JavaClass("uk.ac.open.t802.network.walker.member.fixture", "ValueWriter", Color.BLUE);
        node5 = new JavaClass("uk.ac.open.t802.network.walker.member.fixture", "YetAnotherInterface", Color.BLUE);
        node6 = new JavaClass("uk.ac.open.t802.network.walker.member.fixture", "AbstractService", Color.BLUE);
        node7 = new JavaClass("uk.ac.open.t802.network.walker.member.fixture", "FixtureClient", Color.BLUE);
        node8 = new JavaClass("uk.ac.open.t802.network.walker.member.fixture", "FixtureService", Color.BLUE);
        node9 = new JavaClass("uk.ac.open.t802.network.walker.member.fixture", "LamdaClient", Color.BLUE);
        node10 = new JavaClass("uk.ac.open.t802.network.walker.member.fixture", "Person", Color.BLUE);

    }

    @Test
    public void testMemberWalker() throws Exception {
        DataRepresentationGenerator dataRepresentationGenerator = new DataRepresentationGenerator();

        DirectoryWalker directoryWalker = new DirectoryWalker();

        boolean multigraph = false;

        directoryWalker.walkResources(multigraph);

        directoryWalker.walkDirectory(ROOT_PATH, true, multigraph);

        FileUtils.exportToFile(STORE_PATH + "/log.txt", directoryWalker.graph.getLogging());
        FileUtils.exportToFile(STORE_PATH + "/failedEdges.txt", directoryWalker.graph.getFailedEdges());
        FileUtils.exportToFile(STORE_PATH + "/variables.txt", dataRepresentationGenerator.toVariables(directoryWalker.graph));

        FileUtils.exportToFile(STORE_PATH + "/dependencyNodes.txt", dataRepresentationGenerator.toNodes(directoryWalker.dependencyGraph));
        FileUtils.exportToFile(STORE_PATH + "/dependencyGraph.dot", dataRepresentationGenerator.toOmniGraph(directoryWalker.dependencyGraph));
        FileUtils.exportToFile(STORE_PATH + "/dependencyGraphNumbered.dot", dataRepresentationGenerator.toOmniGraphNumbered(directoryWalker.dependencyGraph));
        FileUtils.exportToFile(STORE_PATH + "/dependencyStatistics.txt", dataRepresentationGenerator.toStatistics(directoryWalker.dependencyGraph));
        FileUtils.exportToFile(STORE_PATH + "/dependencyBarChart.txt", dataRepresentationGenerator.toBarChart(directoryWalker.dependencyGraph));
        FileUtils.exportToFile(STORE_PATH + "/dependencyBarChart.txt", dataRepresentationGenerator.toBarChart(directoryWalker.dependencyGraph));

        Map<Integer, JavaClass> nodesMap = Maps.newHashMap();
        directoryWalker.dependencyGraph.nodes.forEach(node -> nodesMap.put(node.id, node));

        classesIdentical(node1, nodesMap.get(1));
        classesIdentical(node2, nodesMap.get(2));
        classesIdentical(node3, nodesMap.get(3));
        classesIdentical(node4, nodesMap.get(4));
        classesIdentical(node5, nodesMap.get(5));
        classesIdentical(node6, nodesMap.get(6));
        classesIdentical(node7, nodesMap.get(7));
        classesIdentical(node8, nodesMap.get(8));
        classesIdentical(node9, nodesMap.get(9));
        classesIdentical(node10, nodesMap.get(10));

        Map<JavaClass, List<JavaClass>> edges = directoryWalker.dependencyGraph.edges;

        Assert.assertTrue(edges.get(node7).contains(node8));
        Assert.assertTrue(edges.get(node7).contains(node5));
        Assert.assertTrue(edges.get(node7).contains(node1));
        Assert.assertTrue(edges.get(node7).contains(node3));
        Assert.assertTrue(edges.get(node7).size() == 4);
        Assert.assertTrue(edges.get(node8).contains(node7));
        Assert.assertTrue(edges.get(node8).contains(node2));
        Assert.assertTrue(edges.get(node8).contains(node9));
        Assert.assertTrue(edges.get(node8).contains(node10));
        Assert.assertTrue(edges.get(node8).size() == 4);
        Assert.assertTrue(edges.get(node9).contains(node1));
        Assert.assertTrue(edges.get(node9).contains(node8));
        Assert.assertTrue(edges.get(node9).size() == 2);
        Assert.assertTrue(edges.size() == 3);

    }

    private void classesIdentical(JavaClass expected, JavaClass actual) {
        Assert.assertEquals(expected.className, actual.className);
        Assert.assertEquals(expected.packageName, actual.packageName);
        Assert.assertEquals(expected.color, actual.color);

    }

}