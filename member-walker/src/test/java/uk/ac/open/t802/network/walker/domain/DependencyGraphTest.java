package uk.ac.open.t802.network.walker.domain;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Georgia Bogdanou
 */
public class DependencyGraphTest {


    private Graph graph;
    private DependencyGraph dependencyGraph;

    @Before
    public void setUp(){

        graph = new Graph();
        dependencyGraph = new DependencyGraph(new IdGenerator(), graph, false);
    }


    @Test
    public void testGraphFindsNode() {
        JavaClass node1 = new JavaClass("com.package.se", "UtilityCalculator", Color.ORANGE);
        JavaClass node2 = new JavaClass("com.package.se.things", "Packager", Color.RED);
        JavaClass node3 = new JavaClass("com.package.se.interfaces", "NiceInterface", Color.GREEN);

        dependencyGraph.addNode(node1);
        dependencyGraph.addNode(node2);
        dependencyGraph.addNode(node3);

        Assert.assertTrue(dependencyGraph.nodeExists(node1));
        Assert.assertTrue(dependencyGraph.nodeExists(node2));
        Assert.assertTrue(dependencyGraph.nodeExists(node3));

        Assert.assertEquals(dependencyGraph.getNode(node1).className, node1.className);
        Assert.assertEquals(dependencyGraph.getNode(node1).packageName, node1.packageName);
        Assert.assertEquals(dependencyGraph.getNode(node1).color, node1.color);
        Assert.assertTrue(dependencyGraph.getNode(node1).id == 1);

        Assert.assertEquals(dependencyGraph.getNode(node2).className, node2.className);
        Assert.assertEquals(dependencyGraph.getNode(node2).packageName, node2.packageName);
        Assert.assertEquals(dependencyGraph.getNode(node2).color, node2.color);
        Assert.assertTrue(dependencyGraph.getNode(node2).id == 2);

        Assert.assertEquals(dependencyGraph.getNode(node3).className, node3.className);
        Assert.assertEquals(dependencyGraph.getNode(node3).packageName, node3.packageName);
        Assert.assertEquals(dependencyGraph.getNode(node3).color, node3.color);
        Assert.assertTrue(dependencyGraph.getNode(node3).id == 3);


        dependencyGraph.addEdge(node1, node2);
        dependencyGraph.addEdge(node1, node3);

        Assert.assertTrue(dependencyGraph.edges.containsKey(node1));
        Assert.assertTrue(dependencyGraph.edges.get(node1).size() == 2);
        Assert.assertTrue(dependencyGraph.edges.get(node1).contains(node2));
        Assert.assertTrue(dependencyGraph.edges.get(node1).contains(node3));


    }
    @Test
    public void testGraphFindsVariable() {
        JavaClass node1 = new JavaClass("com.package.se", "UtilityCalculator", Color.ORANGE);
        JavaClass node2 = new JavaClass("com.package.se.things", "Packager", Color.RED);

        dependencyGraph.addNode(node1);
        dependencyGraph.addNode(node2);


        graph.addVariable("UtilityCalculator", "blob", "Char");
        graph.addVariable("UtilityCalculator", "jack", "String");
        graph.addVariable("Packager", "lack", "String<bulshit>");

        String inheritedVariable = graph.findVariable("UtilityCalculator", "jack");
        String variable = graph.findVariable("UtilityCalculator", "blob");
        Assert.assertEquals(variable, "Char");
        Assert.assertEquals(inheritedVariable, "String");

    }

}
