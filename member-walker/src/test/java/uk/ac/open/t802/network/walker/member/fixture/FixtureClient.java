package uk.ac.open.t802.network.walker.member.fixture;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * Author youlib
 * Since version 1.0
 */
public class FixtureClient {


    FixtureServiceInterface service = new FixtureService();

    public void massiveDelivery() {

        List<String> deliveryList = Lists.newArrayList("pizza", "cola");
        deliveryList.forEach(item -> service.getMeDelivery(item));

    }

    public void parseMyArguments(String someJobName, int count) {

        service.doSomething(service.getMeDelivery(someJobName), count(), "haha!");

        service.someBeans();

    }

    public void useFixtureService() {
        String potato = "potatoooo!";
        service.doSomething("Hello world", 5, "bob");

        String potatoes = service.getMeDelivery("tomatoes");

        FixtureService fixtureServiceLala = new FixtureService();
        fixtureServiceLala.doLaLa();

    }


    public int count() {
        return 5;
    }

    public void useSomething() {

        String eggplant = new FixtureService().doSomethingElse("eggplant");
        service.makeCoffee();

    }


}
