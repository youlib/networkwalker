package uk.ac.open.t802.network.walker.member;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import uk.ac.open.t802.network.walker.domain.Color;
import uk.ac.open.t802.network.walker.domain.Member;
import java.util.List;
import java.util.Map;

/**
 * Author youlib
 * Since version 1.0
 */
@Ignore
public class DirectoryWalkerMemberTest {

    private static final String STORE_PATH = "results";
    //    private static final String ROOT_PATH = "member-walker/src/test/java/java/fixture";
//    private static final String ROOT_PATH = "member-walker/src/test/java/java";
    private static final String ROOT_PATH = "member-walker/src/test/java/uk/ac/open/t802/network/walker/member/fixture";
//    private static final String ROOT_PATH = "member-walker/src/test/java/uk/ac/open/t802/network/walker/member/fixture/subset/supersubset";

    private Member member1;
    private Member member2;
    private Member member3;
    private Member member4;
    private Member member5;
    private Member member6;
    private Member member7;
    private Member member8;
    private Member member9;
    private Member member10;
    private Member member11;
    private Member member12;
    private Member member13;
    private Member member14;
    private Member member15;
    private Member member16;
    private Member member17;
    private Member member18;
    private Member member19;
    private Member member20;
    private Member member21;
    private Member member22;
    private Member member23;
    private Member member24;
    private Member member25;
    private Member member26;
    private Member member27;
    private Member member28;
    private Member member29;
    private Member member30;
    private Member member31;
    private Member member32;

    @Before
    public void setUp() {

        member1 = new Member.Builder(Color.BLUE).withClassName("FixtureServiceInterface").withReturnType("void").withMethodName("someBeans").withMethodArguments(Lists.newArrayList()).build();
        member2 = new Member.Builder(Color.BLUE).withClassName("FixtureServiceInterface").withReturnType("void").withMethodName("doSomething").withMethodArguments(Lists.newArrayList("String", "Integer", "String")).build();
        member3 = new Member.Builder(Color.BLUE).withClassName("FixtureServiceInterface").withReturnType("String").withMethodName("getMeDelivery").withMethodArguments(Lists.newArrayList("String")).build();
        member4 = new Member.Builder(Color.BLUE).withClassName("FixtureWriter").withReturnType("List<?>").withMethodName("doPotatoes").withMethodArguments(Lists.newArrayList("FixtureWriter<?>", "FixtureService")).build();
        member5 = new Member.Builder(Color.BLUE).withClassName("LalaInterface").withReturnType("void").withMethodName("doLaLa").withMethodArguments(Lists.newArrayList()).build();
        member6 = new Member.Builder(Color.BLUE).withClassName("ValueWriter").withReturnType("List<?>").withMethodName("write").withMethodArguments(Lists.newArrayList("List<BigDecimal>")).build();
        member7 = new Member.Builder(Color.BLUE).withClassName("YetAnotherInterface").withReturnType("void").withMethodName("makeCoffee").withMethodArguments(Lists.newArrayList()).build();
        member8 = new Member.Builder(Color.BLUE).withClassName("YetAnotherInterface").withReturnType("void").withMethodName("makeTea").withMethodArguments(Lists.newArrayList()).build();
        member9 = new Member.Builder(Color.BLUE).withClassName("AbstractService").withReturnType("void").withMethodName("makeCoffee").withMethodArguments(Lists.newArrayList()).build();
        member10 = new Member.Builder(Color.BLUE).withClassName("FixtureClient").withReturnType("void").withMethodName("massiveDelivery").withMethodArguments(Lists.newArrayList()).build();
        member11 = new Member.Builder(Color.BLUE).withClassName("FixtureClient").withReturnType("void").withMethodName("parseMyArguments").withMethodArguments(Lists.newArrayList("String", "Integer")).build();
        member12 = new Member.Builder(Color.BLUE).withClassName("FixtureClient").withReturnType("void").withMethodName("useFixtureService").withMethodArguments(Lists.newArrayList()).build();
        member13 = new Member.Builder(Color.BLUE).withClassName("FixtureClient").withReturnType("Integer").withMethodName("count").withMethodArguments(Lists.newArrayList()).build();
        member14 = new Member.Builder(Color.BLUE).withClassName("FixtureClient").withReturnType("void").withMethodName("useSomething").withMethodArguments(Lists.newArrayList()).build();
        member15 = new Member.Builder(Color.BLUE).withClassName("FixtureService").withReturnType("void").withMethodName("useRerson").withMethodArguments(Lists.newArrayList()).build();
        member16 = new Member.Builder(Color.BLUE).withClassName("FixtureService").withReturnType("void").withMethodName("doSomething").withMethodArguments(Lists.newArrayList("String", "Integer", "String")).build();
        member17 = new Member.Builder(Color.BLUE).withClassName("FixtureService").withReturnType("String").withMethodName("getMeDelivery").withMethodArguments(Lists.newArrayList("String")).build();
        member18 = new Member.Builder(Color.BLUE).withClassName("FixtureService").withReturnType("String").withMethodName("doSomethingElse").withMethodArguments(Lists.newArrayList("String")).build();
        member19 = new Member.Builder(Color.BLUE).withClassName("FixtureService").withReturnType("List<?>").withMethodName("evaluate").withMethodArguments(Lists.newArrayList("FixtureWriter<?>", "LamdaClient")).build();
        member20 = new Member.Builder(Color.BLUE).withClassName("FixtureService.ConcreteWriter").withReturnType("List<Integer>").withMethodName("write").withMethodArguments(Lists.newArrayList("List<BigDecimal>")).build();
        member21 = new Member.Builder(Color.BLUE).withClassName("LamdaClient").withReturnType("void").withMethodName("lamdaSomehting").withMethodArguments(Lists.newArrayList()).build();
        member22 = new Member.Builder(Color.BLUE).withClassName("LamdaClient.MyInnerMagicWorld").withReturnType("String").withMethodName("magic").withMethodArguments(Lists.newArrayList()).build();
        member23 = new Member.Builder(Color.BLUE).withClassName("LamdaClient").withReturnType("LamdaClient").withMethodName("byName").withMethodArguments(Lists.newArrayList("String")).build();
        member24 = new Member.Builder(Color.BLUE).withClassName("LamdaClient").withReturnType("LamdaClient").withMethodName("byHeight").withMethodArguments(Lists.newArrayList("Integer")).build();
        member25 = new Member.Builder(Color.BLUE).withClassName("LamdaClient").withReturnType("Person").withMethodName("get").withMethodArguments(Lists.newArrayList()).build();
        member26 = new Member.Builder(Color.BLUE).withClassName("Person").withReturnType("List<?>").withMethodName("evaluate").withMethodArguments(Lists.newArrayList("ValueWriter<?>")).build();
        member27 = new Member.Builder(Color.BLUE).withClassName("Person").withReturnType("Integer").withMethodName("addStuff").withMethodArguments(Lists.newArrayList("Map<String,String>")).build();
        member28 = new Member.Builder(Color.BLUE).withClassName("Person").withReturnType("void").withMethodName("addStuff").withMethodArguments(Lists.newArrayList("List<String>")).build();
        member29 = new Member.Builder(Color.BLUE).withClassName("Person.PersonBuilder").withReturnType("PersonBuilder").withMethodName("withAge").withMethodArguments(Lists.newArrayList("Integer")).build();
        member30 = new Member.Builder(Color.BLUE).withClassName("Person.PersonBuilder").withReturnType("PersonBuilder").withMethodName("withLastName").withMethodArguments(Lists.newArrayList("String")).build();
        member31 = new Member.Builder(Color.BLUE).withClassName("Person.PersonBuilder").withReturnType("Person").withMethodName("build").withMethodArguments(Lists.newArrayList()).build();
        member32 = new Member.Builder(Color.BLUE).withClassName("Person").withReturnType("String").withMethodName("isAGoodPerson").withMethodArguments(Lists.newArrayList("FixtureWriter")).build();

    }

    @Test
    public void testMemberWalker() throws Exception {
        DataRepresentationGenerator dataRepresentationGenerator = new DataRepresentationGenerator();

        Boolean multigraph = false;

        DirectoryWalker directoryWalker = new DirectoryWalker();

        directoryWalker.walkResources(multigraph);

        directoryWalker.walkDirectory(ROOT_PATH, true, multigraph);

        FileUtils.exportToFile(STORE_PATH + "/log.txt", directoryWalker.graph.getLogging());
        FileUtils.exportToFile(STORE_PATH + "/actions.txt", directoryWalker.graph.getActions());
        FileUtils.exportToFile(STORE_PATH + "/failedEdges.txt", directoryWalker.graph.getFailedEdges());
        FileUtils.exportToFile(STORE_PATH + "/variables.txt", dataRepresentationGenerator.toVariables(directoryWalker.graph));

        FileUtils.exportToFile(STORE_PATH + "/memberNodes.txt", dataRepresentationGenerator.toNodes(directoryWalker.memberGraph));
        FileUtils.exportToFile(STORE_PATH + "/memberGraph.dot", dataRepresentationGenerator.toOmniGraph(directoryWalker.memberGraph));
        FileUtils.exportToFile(STORE_PATH + "/memberGraphNumbered.dot", dataRepresentationGenerator.toOmniGraphNumbered(directoryWalker.memberGraph));
        FileUtils.exportToFile(STORE_PATH + "/memberStatistics.txt", dataRepresentationGenerator.toStatistics(directoryWalker.memberGraph));
        FileUtils.exportToFile(STORE_PATH + "/memberBarChart.txt", dataRepresentationGenerator.toBarChart(directoryWalker.memberGraph));
        FileUtils.exportToFile(STORE_PATH + "/memberFlatResults.txt", dataRepresentationGenerator.toFlatResults(directoryWalker.memberGraph));
        FileUtils.exportToFile(STORE_PATH + "/memberCytoscapeGraph.xgmml", dataRepresentationGenerator.toXGMML(directoryWalker.memberGraph));

        FileUtils.exportToFile(STORE_PATH + "/resourceVariables.txt", dataRepresentationGenerator.toVariables(directoryWalker.resourcesGraph));
        FileUtils.exportToFile(STORE_PATH + "/resourceMemberNodes.txt", dataRepresentationGenerator.toNodes(directoryWalker.resourcesMemberGraph));

        Map<Integer, Member> nodesMap = Maps.newHashMap();
        directoryWalker.memberGraph.getNodes().forEach(member -> nodesMap.put(member.id, member));

        membersIdentical(member1, nodesMap.get(1));
        membersIdentical(member2, nodesMap.get(2));
        membersIdentical(member3, nodesMap.get(3));
        membersIdentical(member4, nodesMap.get(4));
        membersIdentical(member5, nodesMap.get(5));
        membersIdentical(member6, nodesMap.get(6));
        membersIdentical(member7, nodesMap.get(7));
        membersIdentical(member8, nodesMap.get(8));
        membersIdentical(member9, nodesMap.get(9));
        membersIdentical(member10, nodesMap.get(10));
        membersIdentical(member11, nodesMap.get(11));
        membersIdentical(member12, nodesMap.get(12));
        membersIdentical(member13, nodesMap.get(13));
        membersIdentical(member14, nodesMap.get(14));
        membersIdentical(member15, nodesMap.get(15));
        membersIdentical(member16, nodesMap.get(16));
        membersIdentical(member17, nodesMap.get(17));
        membersIdentical(member18, nodesMap.get(18));
        membersIdentical(member19, nodesMap.get(19));
        membersIdentical(member20, nodesMap.get(20));
        membersIdentical(member21, nodesMap.get(21));
        membersIdentical(member22, nodesMap.get(22));
        membersIdentical(member23, nodesMap.get(23));
        membersIdentical(member24, nodesMap.get(24));
        membersIdentical(member25, nodesMap.get(25));
        membersIdentical(member26, nodesMap.get(26));
        membersIdentical(member27, nodesMap.get(27));
        membersIdentical(member28, nodesMap.get(28));
        membersIdentical(member29, nodesMap.get(29));
        membersIdentical(member30, nodesMap.get(30));
        membersIdentical(member31, nodesMap.get(31));
        membersIdentical(member32, nodesMap.get(32));

        Assert.assertTrue(directoryWalker.memberGraph.getNodes().size() == 32);

        Map<Member, List<Member>> edges = directoryWalker.memberGraph.getEdges();

        Assert.assertTrue(edges.get(member1).contains(member7));
        Assert.assertTrue(edges.get(member1).contains(member8));
        Assert.assertTrue(edges.get(member1).size() == 2);

        Assert.assertTrue(edges.get(member10).contains(member3));
        Assert.assertTrue(edges.get(member10).size() == 1);

        Assert.assertTrue(edges.get(member11).contains(member1));
        Assert.assertTrue(edges.get(member11).contains(member2));
        Assert.assertTrue(edges.get(member11).contains(member3));
        Assert.assertTrue(edges.get(member11).contains(member13));
        Assert.assertTrue(edges.get(member11).size() == 4);

        Assert.assertTrue(edges.get(member12).contains(member2));
        Assert.assertTrue(edges.get(member12).contains(member3));
        Assert.assertTrue(edges.get(member12).contains(member5));
        Assert.assertTrue(edges.get(member12).size() == 3);

        Assert.assertTrue(edges.get(member14).contains(member7));
        Assert.assertTrue(edges.get(member14).contains(member18));
        Assert.assertTrue(edges.get(member14).size() == 2);

        Assert.assertTrue(edges.get(member15).contains(member29));
        Assert.assertTrue(edges.get(member15).contains(member30));
        Assert.assertTrue(edges.get(member15).contains(member31));
        Assert.assertTrue(edges.get(member15).contains(member26));
        Assert.assertTrue(edges.get(member15).contains(member28));
        Assert.assertTrue(edges.get(member15).size() == 5);

        Assert.assertTrue(edges.get(member16).contains(member14));
        Assert.assertTrue(edges.get(member16).size() == 1);

        Assert.assertTrue(edges.get(member18).contains(member22));
        Assert.assertTrue(edges.get(member18).size() == 1);

        Assert.assertTrue(edges.get(member19).contains(member4));
        Assert.assertTrue(edges.get(member19).contains(member23));
        Assert.assertTrue(edges.get(member19).contains(member24));
        Assert.assertTrue(edges.get(member19).contains(member25));
        Assert.assertTrue(edges.get(member19).size() == 4);

        Assert.assertTrue(edges.get(member21).contains(member3));
        Assert.assertTrue(edges.get(member21).contains(member18));
        Assert.assertTrue(edges.get(member21).size() == 2);

        Assert.assertTrue(edges.get(member32).contains(member18));
        Assert.assertTrue(edges.get(member32).size() == 1);

        Assert.assertTrue(edges.size() == 11);

    }

    private void membersIdentical(Member expected, Member actual) {
        Assert.assertEquals(expected.className, actual.className);
        Assert.assertEquals(expected.methodName, actual.methodName);
        Assert.assertEquals(expected.methodArguments, actual.methodArguments);
        Assert.assertEquals(expected.returnType, actual.returnType);

    }

}