package uk.ac.open.t802.network.walker.member.fixture;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.Map;

/**
 * @author Georgia Bogdanou
 */
public class Person <T>{
    String firstname;
    int age;
    String lastname;


    public <ValueType> List<ValueType> evaluate(ValueWriter<ValueType> sampleWriter) {
        List<ValueType> list = Lists.newArrayList();
        return list;

    }

    public int addStuff(Map<String, String> myMap) {

        return 1;
    }

    public void addStuff(List<String> myList) {

    }


    public static class PersonBuilder {

        String firstname;
        int age;
        String lastname;

        public PersonBuilder(String firstname) {
            this.firstname = firstname;
        }

        public PersonBuilder withAge(int age) {
            this.age = age;
            return this;
        }

        public PersonBuilder withLastName(String lastName) {
            this.lastname = lastName;
            return this;
        }

        public Person build() {

            Person person = new Person();
            person.firstname = this.firstname;
            person.lastname = this.lastname;
            person.age = this.age;
            return person;
        }
    }


    public String isAGoodPerson(FixtureWriter<T> meny) {
        return FixtureService.doSomethingElse("hungry");
    }
}
