package uk.ac.open.t802.network.walker.member.fixture;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * @author Georgia Bogdanou
 */
public interface FixtureWriter<ValueType> {

    default List<ValueType> doPotatoes(FixtureWriter<ValueType> sampleWriter, FixtureService fixtureService){
        List<ValueType> list = Lists.newLinkedList();
        return list;
    }

}