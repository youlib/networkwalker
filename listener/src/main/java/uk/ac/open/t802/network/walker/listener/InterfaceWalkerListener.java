package uk.ac.open.t802.network.walker.listener;

import com.google.common.collect.Lists;
import uk.ac.open.t802.network.walker.domain.*;
import uk.ac.open.t802.network.walker.grammar.JavaParser;

import java.util.List;

/**
 * Author youlib
 * Since version 1.0
 */
public class InterfaceWalkerListener extends MemberBaseListener {

    private final DependencyGraph dependencyGraph;
    private final MemberGraph memberGraph;

    private String className;
    private List<String> typeParameters = Lists.newArrayList();

    public InterfaceWalkerListener(DependencyGraph dependencyGraph, MemberGraph memberGraph, String className) {
        this.memberGraph = memberGraph;
        this.dependencyGraph = dependencyGraph;
        this.className = className;
    }

    @Override
    public void enterInterfaceDeclaration(JavaParser.InterfaceDeclarationContext ctx) {
        if (ctx.normalInterfaceDeclaration() != null && ctx.normalInterfaceDeclaration().typeParameters() != null && ctx.normalInterfaceDeclaration().typeParameters().typeParameterList() != null) {
            ctx.normalInterfaceDeclaration().typeParameters().typeParameterList().typeParameter().forEach(typeParameterContext -> typeParameters.add(typeParameterContext.getText()));
        }
    }


    @Override
    public void enterInterfaceMethodDeclaration(JavaParser.InterfaceMethodDeclarationContext ctx) {

        Member member = processMethodDeclaration(ctx, className, dependencyGraph.graph);
        if (member != null) {

        for (String typeParameter : typeParameters){
            if (member.returnType.contains(typeParameter)){
                member.returnType = member.returnType.replace(typeParameter, "?");
            }
            List<String> newMethodArguments = Lists.newArrayList();
            for (int i = 0; i < member.methodArguments.size(); i++ ){
               if (member.methodArguments.get(i).contains(typeParameter)){
                   newMethodArguments.add(member.methodArguments.get(i).replace(typeParameter, "?"));
               }
                else{
                   newMethodArguments.add(member.methodArguments.get(i));
               }
            }
            member.methodArguments = newMethodArguments;
        }

            dependencyGraph.addNode(new JavaClass(packageName, className, Color.GREEN));
            memberGraph.addNode(member);
        }
    }

}
