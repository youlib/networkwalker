package uk.ac.open.t802.network.walker.listener.context;

import org.apache.commons.lang3.StringUtils;
import uk.ac.open.t802.network.walker.grammar.JavaParser;

/**
 * @author Georgia Bogdanou
 */
public class ContextParser {

    public static String parseLiteral(JavaParser.LiteralContext ctx) {
        if (ctx.CharacterLiteral() != null) {
            return "Char";

        }
        if (ctx.StringLiteral() != null) {
            return "String";
        }
        if (ctx.BooleanLiteral() != null) {
            return "Boolean";
        }
        if (ctx.FloatingPointLiteral() != null) {
            return "Float";
        }
        if (ctx.IntegerLiteral() != null) {
            return "Integer";
        }
        if (ctx.NullLiteral() != null) {
            return "Null";
        }

        return null;
    }

    public static String deductType(JavaParser.ResultContext result, String genericType) {
        if (result == null) {
            return null;
        }
        if (!StringUtils.isEmpty(genericType)) {
            return result.getText().replaceAll(genericType, "<?>");
        } else {
            return result.getText();
        }
    }

    public static String deductType(JavaParser.UnannTypeContext result, String genericType) {
        if (result == null) {
            return null;
        }
        if (!StringUtils.isEmpty(genericType)) {
            return result.getText().replaceAll(genericType, "<?>");
        }
        if (result.getText().contains("<") && result.getText().contains(">")
            && result.getText().substring(result.getText().indexOf("<") + 1, result.getText().indexOf(">")).length() == 1) {
            return result.getText().substring(0, result.getText().indexOf("<")).replaceAll("<", "<?>");
        } else {
            return result.getText();
        }
    }

    public static String deductVariableName(JavaParser.FormalParameterContext fpc) {
        return fpc.variableDeclaratorId().getText();
    }

}


