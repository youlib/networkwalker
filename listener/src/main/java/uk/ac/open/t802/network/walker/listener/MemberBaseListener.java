package uk.ac.open.t802.network.walker.listener;

import com.google.common.collect.Lists;
import uk.ac.open.t802.network.walker.domain.Color;
import uk.ac.open.t802.network.walker.domain.Graph;
import uk.ac.open.t802.network.walker.domain.Member;
import uk.ac.open.t802.network.walker.grammar.JavaBaseListener;
import uk.ac.open.t802.network.walker.grammar.JavaParser;
import uk.ac.open.t802.network.walker.listener.context.ContextParser;
import java.util.List;

/**
 * Author youlib
 * Since version 1.0
 */
public class MemberBaseListener extends JavaBaseListener {

    protected String packageName;


    @Override
    public void enterPackageDeclaration(JavaParser.PackageDeclarationContext ctx) {
        packageName = ctx.getText().replace("package", "").replace(";", "");
    }

    protected String deductClassName(JavaParser.NormalClassDeclarationContext ctx, String className) {

        if (ctx.Identifier() != null && !ctx.Identifier().getText().equals(className)) {
            return  className + "." + ctx.Identifier().getText();
        }
        return className;
    }

    protected Member processMethodDeclaration(JavaParser.InterfaceMethodDeclarationContext ctx, String className, Graph graph) {
        if (!ctx.getText().startsWith("private") && ctx.methodHeader() != null) {
            String methodName = ctx.methodHeader().methodDeclarator().Identifier().getText();
            String genericType = "";
            if (ctx.methodHeader().typeParameters() != null) {
                genericType = ctx.methodHeader().typeParameters().getText();
            }
            List<String> methodArguments = processMethodArguments(ctx.methodHeader(), className, graph, genericType);

            String returnType = ContextParser.deductType(ctx.methodHeader().result(), genericType);
            return new Member.Builder(Color.BLUE).withClassName(className).withReturnType(returnType).withMethodName(methodName).withMethodArguments(methodArguments).build();
        }
        return null;
    }

    private List<String> processMethodArguments(JavaParser.MethodHeaderContext ctx, String className, Graph graph, String genericType) {
        List<String> argumentList = Lists.newArrayList();

        if ((ctx.methodDeclarator().formalParameterList() != null) && (ctx.methodDeclarator().formalParameterList() != null)) {
            if (ctx.methodDeclarator().formalParameterList().formalParameters() != null && !ctx.methodDeclarator().formalParameterList().formalParameters().isEmpty()) {

                ctx.methodDeclarator().formalParameterList().formalParameters().formalParameter().forEach(formalParameterContext -> {
                    if (formalParameterContext.unannType() != null) {

                        String type = ContextParser.deductType(formalParameterContext.unannType(), genericType);
                        argumentList.add(type);
                        String variableName = ContextParser.deductVariableName(formalParameterContext);
                        graph.addVariable(className, variableName, type);
                    }
                });
            }
            if (ctx.methodDeclarator().formalParameterList().lastFormalParameter() != null
                && ctx.methodDeclarator().formalParameterList().lastFormalParameter().formalParameter() != null
                && ctx.methodDeclarator().formalParameterList().lastFormalParameter().formalParameter().unannType() != null) {

                JavaParser.UnannTypeContext utc = ctx.methodDeclarator().formalParameterList().lastFormalParameter().formalParameter().unannType();

                String type = ContextParser.deductType(utc, genericType);
                argumentList.add(type);

                String variableName = ContextParser.deductVariableName(ctx.methodDeclarator().formalParameterList().lastFormalParameter().formalParameter());
                graph.addVariable(className, variableName, type);

            }
        }

        return argumentList;
    }


    protected Member processMethodDeclaration(JavaParser.MethodDeclarationContext ctx, String className, Graph graph) {
        if (!ctx.getText().startsWith("private") && ctx.methodHeader() != null) {
            String genericType = "";
            if (ctx.methodHeader().typeParameters() != null) {
                genericType = ctx.methodHeader().typeParameters().getText();
            }
            String methodName = ctx.methodHeader().methodDeclarator().Identifier().getText();
            List<String> methodArguments = processMethodArguments(ctx.methodHeader(), className, graph, genericType);
            String returnType;
            if (ctx.methodHeader().result().unannType() != null) {
                returnType = ContextParser.deductType(ctx.methodHeader().result().unannType(), genericType);
            } else {
                returnType = ctx.methodHeader().result().getText();
            }
            return new Member.Builder(Color.BLUE).withMethodArguments(methodArguments).withClassName(className).withReturnType(returnType).withMethodName(methodName).template();
        }
        return null;
    }


}
