package uk.ac.open.t802.network.walker.listener.context;

import com.google.common.collect.Lists;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.apache.commons.lang3.StringUtils;
import uk.ac.open.t802.network.walker.domain.*;
import uk.ac.open.t802.network.walker.grammar.JavaParser;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Georgia Bogdanou
 */
public class ContextAnalyser {

    private final Graph graph;
    private final MemberGraph memberGraph;
    private final MemberGraph resourcesGraph;

    public ContextAnalyser(Graph graph, MemberGraph memberGraph, MemberGraph resourcesGraph) {
        this.graph = graph;
        this.memberGraph = memberGraph;
        this.resourcesGraph = resourcesGraph;
    }

    /**
     * The entry point in context analyser
     *
     * @param statement it can be a classDeclarationContext -> outOfScope (ClassWalker's task)
     *                  it can be a LocalVariableDeclarationStatement
     *                  it can be a BlockStatementContext
     * @param member
     */
    public void processMethodInvocation(JavaParser.BlockStatementContext statement, final Member member) {
        List<Member> invokedMembers = Lists.newArrayList();
        if (statement == null) {
            return;
        }
        if (statement.statement() != null) {
            invokedMembers = processStatementContext(statement.statement(), member);
        }
        if (statement.localVariableDeclarationStatement() != null) {
            invokedMembers = processLocalVariableDeclarationStatement(statement, member);
        }
        if (invokedMembers != null && !invokedMembers.isEmpty()) {
            invokedMembers.forEach(invokedMember -> memberGraph.addEdge(member, invokedMember));
        }
    }

    /**
     * @param statement it can be a classDeclarationContext -> outOfScope (ClassWalker's task)
     *                  it can be a BlockStatementContext
     *                  it can be a LocalVariableDeclarationStatementContext
     * @param member
     */
    private List<Member> processLocalVariableDeclarationStatement(JavaParser.BlockStatementContext statement, final Member member) {
        if (statement.statement() != null) {
            return processStatementContext(statement.statement(), member);
        }
        if (statement.localVariableDeclarationStatement() != null) {
            return processLocalVariableDeclarationStatement(statement.localVariableDeclarationStatement(), member);
        }
        return null;
    }

    /**
     * @param statement it can be ForStatement
     *                  it can be WhileStatement
     *                  it can be ifThenElseStatement
     *                  it can be ifThenStatement
     *                  it can be StatementWithoutTrailingSubstatement
     *                  it can be LabeledStatement
     * @param member
     */

    private List<Member> processStatementContext(JavaParser.StatementContext statement, final Member member) {
        if (statement.forStatement() != null) {
            return processForStatement(statement.forStatement(), member);
        }
        if (statement.whileStatement() != null) {
            return processWhileStatement(statement.whileStatement(), member);
        }
        if (statement.ifThenElseStatement() != null) {
            return processIfThenElseStatement(statement.ifThenElseStatement(), member);
        }
        if (statement.ifThenStatement() != null) {
            return processIfThenStatement(statement.ifThenStatement(), member);
        }
        if (statement.statementWithoutTrailingSubstatement() != null) {
            return processStatementWithoutTrailingSubstatement(statement.statementWithoutTrailingSubstatement(), member);
        }
        if (statement.labeledStatement() != null) {
            return processLabeledStatement(statement.labeledStatement(), member);
        }
        return null;
    }

    /**
     * @param context it can be expressionContext
     *                it can be Statement
     * @param member
     */
    private List<Member> processIfThenStatement(JavaParser.IfThenStatementContext context, Member member) {
        if (context.expression() != null) {
            return processExpressionContext(context.expression(), member);
        }
        if (context.statement() != null) {
            return processStatementContext(context.statement(), member);
        }
        return null;
    }

    /**
     * @param context it can be expressionContext
     *                it can be Statement
     *                it can be StatementNoShortIf
     * @param member
     */
    private List<Member> processIfThenElseStatement(JavaParser.IfThenElseStatementContext context, Member member) {
        if (context.expression() != null) {
            return processExpressionContext(context.expression(), member);
        }
        if (context.statement() != null) {
            return processStatementContext(context.statement(), member);
        }
        if (context.statementNoShortIf() != null) {
            return processStatementNoShortIfContext(context.statementNoShortIf(), member);
        }
        return null;

    }

    /**
     * @param context it can be expressionContext
     *                it can be Statement
     * @param member
     */
    private List<Member> processWhileStatement(JavaParser.WhileStatementContext context, Member member) {
        if (context.expression() != null) {
            return processExpressionContext(context.expression(), member);
        }
        if (context.statement() != null) {
            return processStatementContext(context.statement(), member);
        }
        return null;
    }

    /**
     * @param context it can be enhancedForStatement
     *                it can be basicForStatement
     * @param member
     */
    private List<Member> processForStatement(JavaParser.ForStatementContext context, Member member) {
        if (context.enhancedForStatement() != null) {
            return processEnhancedForStatement(context.enhancedForStatement(), member);
        }
        if (context.basicForStatement() != null) {
            return processBasicForStatement(context.basicForStatement(), member);
        }
        return null;
    }

    /**
     * @param context it seems to be always null, cannot find a single example, just logging it then
     * @param member
     */
    private List<Member> processLabeledStatement(JavaParser.LabeledStatementContext context, Member member) {
        graph.appendLogging("JavaParser.LabeledStatementContext " + context.getText(), member);
        return null;
    }

    /**
     * @param context it can be expression
     *                it can be statement
     *                it can be forInit
     *                it can be forUpdate
     * @param member
     */
    private List<Member> processBasicForStatement(JavaParser.BasicForStatementContext context, Member member) {
        if (context.expression() != null) {
            return processExpressionContext(context.expression(), member);
        }
        if (context.statement() != null) {
            return processStatementContext(context.statement(), member);
        }
        if (context.forInit() != null && context.forInit().statementExpressionList() != null && context.forInit().statementExpressionList().statementExpression() != null) {
            context.forInit().statementExpressionList().statementExpression().forEach(statementExpressionContext ->
                processStatementExpressionContext(statementExpressionContext, member));

        }
        if (context.forUpdate() != null &&
            context.forUpdate().statementExpressionList() != null) {
            context.forUpdate().statementExpressionList().statementExpression().forEach(statementExpressionContext ->
                processStatementExpressionContext(statementExpressionContext, member));
        }
        return null;
    }

    /**
     * @param context it can be forStatementNoShortIf
     *                it can be ifThenElseStatementNoShortIf
     *                it can be labeledStatementNoShortIf
     *                it can be statementWithoutTrailingSubstatement
     *                it can be whileStatementNoShortIf
     * @param member
     */
    private List<Member> processStatementNoShortIfContext(JavaParser.StatementNoShortIfContext context, Member member) {
        if (context.forStatementNoShortIf() != null) {
            return processForStatementNoShortIf(context.forStatementNoShortIf(), member);
        }
        if (context.ifThenElseStatementNoShortIf() != null) {
            return processIfThenElseStatementNoShortIf(context.ifThenElseStatementNoShortIf(), member);
        }
        if (context.labeledStatementNoShortIf() != null) {
            return processLabeledStatementNoShortIf(context.labeledStatementNoShortIf(), member);
        }
        if (context.statementWithoutTrailingSubstatement() != null) {
            return processStatementWithoutTrailingSubstatement(context.statementWithoutTrailingSubstatement(), member);
        }
        if (context.whileStatementNoShortIf() != null) {
            return processWhileStatementNoShortIf(context.whileStatementNoShortIf(), member);
        }
        return null;
    }

    /**
     * @param context it can be basicForStatementNoShortIf
     *                it can be enhancedForStatementNoShortIf
     * @param member
     */
    private List<Member> processForStatementNoShortIf(JavaParser.ForStatementNoShortIfContext context, Member member) {
        if (context.basicForStatementNoShortIf() != null) {
            return processBasicForStatementNoShortIf(context.basicForStatementNoShortIf(), member);
        }
        if (context.enhancedForStatementNoShortIf() != null) {
            return processEnhancedForStatementNoShortIf(context.enhancedForStatementNoShortIf(), member);
        }
        return null;
    }

    /**
     * @param context it can be statementNoShortIf
     *                it can be expression
     * @param member
     */
    private List<Member> processWhileStatementNoShortIf(JavaParser.WhileStatementNoShortIfContext context, Member member) {
        if (context.statementNoShortIf() != null) {
            return processStatementNoShortIfContext(context.statementNoShortIf(), member);
        }
        if (context.expression() != null) {
            return processExpressionContext(context.expression(), member);
        }
        return null;
    }

    /**
     * @param context it can be expression
     *                it can be statementNoShortIf
     * @param member
     */
    private List<Member> processIfThenElseStatementNoShortIf(JavaParser.IfThenElseStatementNoShortIfContext context, Member member) {
        if (context.expression() != null) {
            return processExpressionContext(context.expression(), member);
        }
        if (context.statementNoShortIf() != null) {
            List<Member> invokedMembers = Lists.newArrayList();
            context.statementNoShortIf().forEach(statementNoShortIfContext -> addAll(invokedMembers, processStatementNoShortIfContext(statementNoShortIfContext, member)));
            return invokedMembers;
        }
        return null;
    }

    /**
     * @param context it can be expression
     *                it can be statementNoShortIf
     *                it can be forInit
     *                it can be forUpdate
     * @param member
     */
    private List<Member> processBasicForStatementNoShortIf(JavaParser.BasicForStatementNoShortIfContext context, Member member) {
        if (context.expression() != null) {
            return processExpressionContext(context.expression(), member);
        }
        if (context.statementNoShortIf() != null) {
            return processStatementNoShortIfContext(context.statementNoShortIf(), member);
        }
        if (context.forInit() != null) {
            return processForInitContext(context.forInit(), member);
        }
        if (context.forUpdate() != null) {
            return processForUpdateContext(context.forUpdate(), member);
        }
        return null;
    }

    /**
     * @param context it can be only statementExpressionList
     * @param member
     */
    private List<Member> processForUpdateContext(JavaParser.ForUpdateContext context, Member member) {
        if (context.statementExpressionList() != null && context.statementExpressionList().statementExpression() != null) {
            List<Member> invokedMembers = Lists.newArrayList();
            context.statementExpressionList().statementExpression().forEach(statementExpressionContext -> addAll(invokedMembers, processStatementExpressionContext(statementExpressionContext, member)));
            return invokedMembers;
        }
        return null;
    }

    /**
     * @param context it can be assignment
     *                it can be methodInvocation
     *                it can be preIncrementExpression
     *                it can be preDecrementExpression
     *                it can be preDecrementExpression
     *                it can be postIncrementExpression
     *                it can be postDecrementExpression
     *                it can be classInstanceCreationExpression
     * @param member
     */
    private List<Member> processStatementExpressionContext(JavaParser.StatementExpressionContext context, Member member) {
        if (context.assignment() != null) {
            return processAssignmentContext(context.assignment(), member);
        }
        if (context.methodInvocation() != null) {
            return processMethodInvocation(context.methodInvocation(), member);
        }
        if (context.preIncrementExpression() != null) {
            return processPreIncrementExpression(context.preIncrementExpression(), member);
        }
        if (context.preDecrementExpression() != null) {
            return processPreDecrementExpression(context.preDecrementExpression(), member);
        }
        if (context.postIncrementExpression() != null) {
            return processPostIncrementExpression(context.postIncrementExpression(), member);
        }
        if (context.postDecrementExpression() != null) {
            return processPostDecrementExpression(context.postDecrementExpression(), member);
        }
        if (context.classInstanceCreationExpression() != null) {
            return processClassInstanceCreationExpression(context.classInstanceCreationExpression(), member);
        }
        return null;
    }

    //******************************* CONFIRMED ********************************************//

    private List<Member> processMethodInvocation(JavaParser.MethodInvocationContext context, Member member) {
        return null;
    }

    private List<Member> processPreIncrementExpression(JavaParser.PreIncrementExpressionContext context, Member member) {
        return null;

    }

    private List<Member> processPreDecrementExpression(JavaParser.PreDecrementExpressionContext context, Member member) {
        return null;

    }

    private List<Member> processPostIncrementExpression(JavaParser.PostIncrementExpressionContext context, Member member) {
        return null;

    }

    private List<Member> processPostDecrementExpression(JavaParser.PostDecrementExpressionContext context, Member member) {
        return null;

    }

    private List<Member> processClassInstanceCreationExpression(JavaParser.ClassInstanceCreationExpressionContext context, Member member) {
        return null;

    }

    private List<Member> processAssignmentContext(JavaParser.AssignmentContext context, Member member) {
        return null;

    }

    private List<Member> processEnhancedForStatementNoShortIf(JavaParser.EnhancedForStatementNoShortIfContext context, Member member) {
        context.statementNoShortIf();
        if (context.expression() != null) {
            processExpressionContext(context.expression(), member);
        }
//        context.unannType();
//        context.variableModifier();
//        context.variableDeclaratorId();
        return null;

    }

    private List<Member> processForInitContext(JavaParser.ForInitContext context, Member member) {
        if (context.statementExpressionList() != null && context.statementExpressionList().statementExpression() != null) {
            context.statementExpressionList().statementExpression().forEach(statementExpressionContext -> processStatementExpressionContext(statementExpressionContext, member));
        }
//        context.localVariableDeclaration();
        return null;

    }

    private List<Member> processLabeledStatementNoShortIf(JavaParser.LabeledStatementNoShortIfContext context, Member member) {
        if (context.statementNoShortIf() != null) {
            processStatementNoShortIfContext(context.statementNoShortIf(), member);
        }
//        context.Identifier();
        return null;

    }

    /**
     * @param context it can be expression
     *                it can be statement
     *                it can be unannType
     *                it can be variableDeclaratorId
     *                it can be variableModifierList
     * @param member
     */

    private List<Member> processEnhancedForStatement(JavaParser.EnhancedForStatementContext context, Member member) {
        if (context.expression() != null) {
            return processExpressionContext(context.expression(), member);
        }
        if (context.statement() != null) {
            return processStatementContext(context.statement(), member);
        }
        if (context.unannType() != null) {
            graph.appendLogging("JavaParser.EnhancedForStatementContext.unannType " + context.unannType().getText(), member);

        }
        if (context.variableDeclaratorId() != null) {
            graph.appendLogging("JavaParser.EnhancedForStatementContext.variableDeclaratorId " + context.variableDeclaratorId().getText(), member);

        }
        if (context.variableModifier() != null && !context.variableModifier().isEmpty()) {
            graph.appendLogging("JavaParser.EnhancedForStatementContext.variableModifier " + context.variableModifier().toString(), member);

        }
        return null;
    }

    private List<Member> processStatementWithoutTrailingSubstatement(JavaParser.StatementWithoutTrailingSubstatementContext context, Member member) {
        //statement of type "service.doSomething(arg);"
        if (context.expressionStatement() != null
            && context.expressionStatement().statementExpression() != null) {
            JavaParser.StatementExpressionContext sec = context.expressionStatement().statementExpression();
            if (sec.methodInvocation() != null) {
                JavaParser.MethodInvocationContext mic = context.expressionStatement().statementExpression().methodInvocation();
                if (mic.Identifier() != null
                    && mic.typeName() != null
                    && mic.typeName().Identifier() != null) {
                    graph.appendLogging("POINT D.1: " + mic.getText(), member);
                    processMethodInvocation(mic.Identifier().getText(), mic.argumentList(), member, graph.findVariable(member.className, mic.typeName().Identifier().getText()), null);

                }
                if (mic.Identifier() != null
                    && mic.typeName() != null
                    && mic.typeName().packageOrTypeName() != null
                    && mic.typeName().packageOrTypeName().Identifier() != null) {
                    //        System.out.println("br, br,br, boiling coffee");
                    //   deliveryList.forEach(item -> service.getMeDelivery(item));
                    //service.doSomething(Service.getMeDelivery(somejobName, count(), "haha"
                    //person.evaluate(newConcrete
                    graph.appendLogging("POINT D: " + mic.getText(), member);
                    processMethodInvocation(mic.Identifier().getText(), mic.argumentList(), member, graph.findVariable(member.className, mic.typeName().packageOrTypeName().Identifier().getText()), null);
                }
                if (mic.methodName() != null){
                    graph.appendLogging("POINT W: " + mic.getText(), member);
                    processMethodInvocation(mic.methodName().getText(), mic.argumentList(), member, member.className, null);
                }
                if (mic.argumentList() != null
                    && mic.argumentList().expression() != null) {

                    mic.argumentList().expression().forEach(expressionContext -> {
                        if (expressionContext != null) {
                            processExpressionContext(expressionContext, member);
                        }
                    });
                }
            }
            if (sec.assignment() != null
                && sec.assignment().expression() != null) {
                processExpressionContext(sec.assignment().expression(), member);

            }

            logPreIncrementExpression(sec, member);
            logPostIncrementExpression(sec, member);
            logPostDecrementExpression(sec, member);
            logClassInstanceCreationException(sec, member);
        }
        if (context.returnStatement() != null && context.returnStatement().expression() != null) {
            JavaParser.ExpressionContext expressionContext = context.returnStatement().expression();
            if (expressionContext != null) {
                processExpressionContext(expressionContext, member);
            }
        }
        logAssertStatement(context, member);
        logEmptyStatement(context, member);
        if (context.block() != null
            && context.block().blockStatements() != null
            && context.block().blockStatements().blockStatement() != null) {
            context.block().blockStatements().blockStatement().forEach(blockStatementContext -> {
                processMethodInvocation(blockStatementContext, member);
            });
        }
        if (context.switchStatement() != null
            && context.switchStatement().expression() != null) {
            processExpressionContext(context.switchStatement().expression(), member);

        }
        if (context.doStatement() != null
            && context.doStatement().expression() != null) {
            processExpressionContext(context.doStatement().expression(), member);

        }
        logBreakStatement(context, member);
        logContinueStatement(context, member);
        if (context.synchronizedStatement() != null
            && context.synchronizedStatement().expression() != null) {
            processExpressionContext(context.synchronizedStatement().expression(), member);

        }
        if (context.tryStatement() != null
            && context.tryStatement().block() != null
            && context.tryStatement().block().blockStatements() != null
            && context.tryStatement().block().blockStatements().blockStatement() != null) {
            context.tryStatement().block().blockStatements().blockStatement().forEach(blockStatementContext -> {
                processMethodInvocation(blockStatementContext, member);
            });
        }

        if (context.throwStatement() != null
            && context.throwStatement().expression() != null) {
            processExpressionContext(context.throwStatement().expression(), member);
        }
        return null;

    }

    /**
     * @param statement it can only be a LocalVariableDeclarationContext
     * @param member
     */
    private List<Member> processLocalVariableDeclarationStatement(JavaParser.LocalVariableDeclarationStatementContext statement, final Member member) {
        if (statement.localVariableDeclaration() != null) {
            JavaParser.LocalVariableDeclarationContext lvdc = statement.localVariableDeclaration();

            graph.appendLogging("lvdc.unannType() " + lvdc.unannType().getText(), member);
            if (lvdc.variableDeclaratorList() != null) {
                processVariableDeclaratorList(lvdc.variableDeclaratorList(), member);
            }
            if (lvdc.variableModifier() != null && !lvdc.variableModifier().isEmpty()) {
                graph.appendLogging("lvdc.variableModifier() " + lvdc.variableModifier(), member);
            }
            lvdc.variableDeclaratorList().variableDeclarator().forEach(variableDeclaratorContext -> {

                if (isLambaExpression(variableDeclaratorContext)) {
                    JavaParser.LambdaExpressionContext lec = variableDeclaratorContext.variableInitializer().expression().lambdaExpression();
                    processLambdaExpression(lec, member);
                }

                if (variableDeclaratorContext.variableInitializer() != null
                    && variableDeclaratorContext.variableInitializer().expression() != null
                    && variableDeclaratorContext.variableInitializer().expression().assignmentExpression() != null) {

                    JavaParser.AssignmentExpressionContext aec = variableDeclaratorContext.variableInitializer().expression().assignmentExpression();

                    processAssignmentExpressionContext(aec, member);

                }

            });
        }
        return null;
    }

    private List<Member> processVariableDeclaratorList(JavaParser.VariableDeclaratorListContext context, final Member member) {
        if (context != null && !context.isEmpty()) {
            for (JavaParser.VariableDeclaratorContext variableDeclaratorContext : context.variableDeclarator()) {
                List<Member> invokedMembers = Lists.newArrayList();
                addAll(invokedMembers, processVariableDeclaratorContext(variableDeclaratorContext, member));
                return invokedMembers;
            }
        }
        return null;
    }

    private List<Member> processVariableDeclaratorContext(JavaParser.VariableDeclaratorContext context, final Member member) {
        graph.appendLogging("JavaParser.VariableDeclaratorContext() " + context.getText(), member);
        return null;
    }

    //Expression context can be either a Lambda Expression or an Assignment Expression
    private List<Member> processExpressionContext(JavaParser.ExpressionContext expressionContext, final Member member) {
        if (expressionContext.lambdaExpression() != null) {
            return processLambdaExpression(expressionContext.lambdaExpression(), member);
        }
        if (expressionContext.assignmentExpression() != null) {
            return processAssignmentExpressionContext(expressionContext.assignmentExpression(), member);
        }
        return null;
    }

    private List<String> processMethodArguments(JavaParser.ArgumentListContext argumentListContext, String variabeName, final Member member) {
        List<String> arguments = Lists.newArrayList();

        if (argumentListContext != null && !argumentListContext.isEmpty()) {
            for (JavaParser.ExpressionContext expressionContext : argumentListContext.expression()) {
                if (expressionContext.getText().equals("this")) {
                    arguments.add(member.className);
                } else if (variabeName != null) {
                    String listVariabe = graph.findVariable(member.className, variabeName);
                    arguments.add(ParseUtils.getType(listVariabe));
                } else if (expressionContext.lambdaExpression() != null) {

                    processLambdaExpression(expressionContext.lambdaExpression(), member);
                } else if (expressionContext.assignmentExpression() != null) {
                    processExpressionContext(expressionContext.assignmentExpression(), member, variabeName, arguments);
                }
            }
        }
        return ParseUtils.normaliseType(arguments);

    }

    //Lambda Expresssion Context can have a lambda body (fully explored),
    //lambda parameters (not of interest on their own)
    //and expression (fully explored
    private List<Member> processLambdaExpression(JavaParser.LambdaExpressionContext lec, final Member member) {
        if (lec.lambdaBody() != null
            && lec.lambdaBody().block() != null
            && lec.lambdaBody().block().blockStatements() != null
            && lec.lambdaBody().block().blockStatements().blockStatement() != null) {
            lec.lambdaBody().block().blockStatements().blockStatement().forEach(statement -> {
                processMethodInvocation(statement, member);

            });
        }
        if (lec.lambdaBody() != null
            && lec.lambdaBody().expression() != null) {
            processExpressionContext(lec.lambdaBody().expression(), member);
        }
        return null;
    }

    private List<String> processExpressionContext(JavaParser.AssignmentExpressionContext expressionContext, final Member member, String variabeName, List<String> arguments) {
        if (expressionContext != null
            && expressionContext.conditionalExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus().postfixExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus().postfixExpression().expressionName() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus().postfixExpression().expressionName().Identifier() != null) {

            String variable = expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus().postfixExpression().expressionName().Identifier().getText();
            arguments.add(graph.findVariable(member.className, variable));

        } else if (expressionContext != null
            && expressionContext.conditionalExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus().postfixExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus().postfixExpression().primary() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus().postfixExpression().primary().primaryNoNewArray_lfno_primary() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus().postfixExpression().primary().primaryNoNewArray_lfno_primary().classInstanceCreationExpression_lfno_primary() != null) {
            List<TerminalNode> identifiers = expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus().postfixExpression().primary().primaryNoNewArray_lfno_primary().classInstanceCreationExpression_lfno_primary().Identifier();

            String type = "";
            for (TerminalNode identifier : identifiers) {
                if (StringUtils.isEmpty(type)) {
                    type = identifier.getText();
                } else {
                    type = type + "." + identifier.getText();
                }
            }
            String compositeType = graph.findVariable(member.className, type);
            if (StringUtils.isEmpty(compositeType)) {
                arguments.add(type);
            } else {
                arguments.add(compositeType);
            }

        } else if (expressionContext != null
            && expressionContext.conditionalExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus().postfixExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus().postfixExpression().expressionName() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus().postfixExpression().expressionName().ambiguousName() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus().postfixExpression().expressionName().ambiguousName().Identifier() != null) {
            String type = expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus().postfixExpression().expressionName().ambiguousName().Identifier().getText();
            if (type != null) {
                arguments.add(type);
            }
        } else if (expressionContext != null
            && expressionContext.conditionalExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus().postfixExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus().postfixExpression().primary() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus().postfixExpression().primary().primaryNoNewArray_lfno_primary() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus().postfixExpression().primary().primaryNoNewArray_lfno_primary().literal() != null) {
            JavaParser.LiteralContext lc = expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus().postfixExpression().primary().primaryNoNewArray_lfno_primary().literal();
            String literalType = ContextParser.parseLiteral(lc);
            if (literalType != null) {
                arguments.add(literalType);
            }
        } else if (expressionContext != null
            && expressionContext.conditionalExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus().postfixExpression() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus().postfixExpression().primary() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus().postfixExpression().primary().primaryNoNewArray_lfno_primary() != null
            && expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus().postfixExpression().primary().primaryNoNewArray_lfno_primary().methodInvocation_lfno_primary() != null) {

            JavaParser.MethodInvocation_lfno_primaryContext milpc = expressionContext.conditionalExpression().conditionalOrExpression().conditionalAndExpression().inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression().shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus().postfixExpression().primary().primaryNoNewArray_lfno_primary().methodInvocation_lfno_primary();
            String nestedInvocationMethod = null;
            String nestedInvocationVariable = null;
            List<String> nestedInvocationArguments = null;
            String nestedInvocationClass = null;
            if (milpc.Identifier() != null
                && milpc.typeName() != null
                && milpc.typeName().Identifier() != null
                ) {
                nestedInvocationMethod = milpc.Identifier().getText();
                nestedInvocationVariable = milpc.typeName().Identifier().getText();
                nestedInvocationArguments = processMethodArguments(milpc.argumentList(), null, member);
                nestedInvocationClass = graph.findVariable(member.className, nestedInvocationVariable);
            } else if (milpc.methodName() != null) {
                nestedInvocationMethod = milpc.methodName().getText();
                nestedInvocationArguments = processMethodArguments(milpc.argumentList(), null, member);
                nestedInvocationClass = member.className;

            } else {
                graph.appendFailedEdge("FAILED METHOD CALL: " + milpc.getText());

            }
            Member inputMember = new Member.Builder(Color.BLUE).withMethodArguments(nestedInvocationArguments).withClassName(nestedInvocationClass).withMethodName(nestedInvocationMethod).template();

            Member toMember = memberGraph.findNode(inputMember, true);
            if (toMember == null) {
                toMember = resourcesGraph.findNode(inputMember, true);
            }
            if (toMember != null) {
                arguments.add(toMember.returnType);
                memberGraph.addEdge(member, toMember);
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("FAILED METHOD CALL: ");
                sb.append(" nestedInvocationClass : " + nestedInvocationClass);
                sb.append(" nestedInvocationMethod : " + nestedInvocationMethod);
                sb.append(" nestedInvocationArguments : " + nestedInvocationArguments);
                sb.append(" className : " + member.className);
                graph.appendFailedEdge(sb.toString());

            }
        } else {
            arguments.add(graph.findVariable(member.className, expressionContext.getText()));
        }

        return arguments;
    }

    private List<Member> processMethodInvocation(String invokedMethod, JavaParser.ArgumentListContext argumentListContext, final Member member, String invokedClass, String variabeName) {
        Member fromNode = memberGraph.findNode(member, false);
        if (fromNode == null) {
            graph.appendFailedEdge("SOMETHING TERRIBLE HAPPENED, and member was null. This is what we know: " + member);
            return null;
        }
        List<String> arguments = processMethodArguments(argumentListContext, variabeName, fromNode);

        Member inputToNode = new Member.Builder(Color.BLUE).withClassName(ParseUtils.normaliseClass(invokedClass)).withMethodName(invokedMethod).withMethodArguments(arguments).template();

        Member toNode = memberGraph.findNode(inputToNode, true);
        if (toNode != null) {
            memberGraph.addEdge(fromNode, toNode);
        } else {
            appendFailedEdge(member, inputToNode);
        }

        return null;
    }

    private void appendFailedEdge(Member inputFromNode, Member inputToNode) {
        Member from = resourcesGraph.findNode(inputFromNode, true);
        Member to = resourcesGraph.findNode(inputToNode, true);
        if ((from == null) && (to != null)
            | (from != null) && (to == null)
            ) {
            StringBuilder sb = new StringBuilder();
            sb.append("FAILED EDGE: ");
            sb.append(" from : " + inputFromNode);
            sb.append(" to : " + inputToNode);
            graph.appendFailedEdge(sb.toString());
        }
    }

    private List<Member> processAssignmentExpressionContext(JavaParser.AssignmentExpressionContext aec, final Member member) {
        if (aec.conditionalExpression() != null && aec.conditionalExpression().conditionalOrExpression() != null && aec.conditionalExpression().conditionalOrExpression().conditionalAndExpression() != null) {
            JavaParser.ConditionalAndExpressionContext caec = aec.conditionalExpression().conditionalOrExpression().conditionalAndExpression();
            JavaParser.RelationalExpressionContext rec = caec.inclusiveOrExpression().exclusiveOrExpression().andExpression().equalityExpression().relationalExpression();

            if (rec.shiftExpression() != null
                && rec.shiftExpression().additiveExpression() != null
                && rec.shiftExpression().additiveExpression().multiplicativeExpression() != null
                && rec.shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression() != null
                && rec.shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus() != null
                && rec.shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus().postfixExpression() != null
                && rec.shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus().postfixExpression().primary() != null) {

                JavaParser.PrimaryContext pc = rec.shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus().postfixExpression().primary();

                List<JavaParser.PrimaryNoNewArray_lf_primaryContext> list = pc.primaryNoNewArray_lf_primary();
                list.forEach(pnna -> processPrimaryNoNewArraylfprimaryContext(pc, pnna, member));

                if (pc.primaryNoNewArray_lfno_primary() != null && pc.primaryNoNewArray_lfno_primary().methodInvocation_lfno_primary() != null) {
                    JavaParser.MethodInvocation_lfno_primaryContext ctx = rec.shiftExpression().additiveExpression().multiplicativeExpression().unaryExpression().unaryExpressionNotPlusMinus().postfixExpression().primary().primaryNoNewArray_lfno_primary().methodInvocation_lfno_primary();

                    if (ctx.Identifier() != null
                        && ctx.typeName() != null
                        && ctx.typeName().Identifier() != null) {
                        graph.appendLogging("POINT C: " + pc.getText(), member);
                        processMethodInvocation(ctx.Identifier().getText(), ctx.argumentList(), member, graph.findVariable(member.className, ctx.typeName().Identifier().getText()), null);

                        if (pc.primaryNoNewArray_lf_primary() != null && !pc.primaryNoNewArray_lf_primary().isEmpty()) {
                            for (JavaParser.PrimaryNoNewArray_lf_primaryContext pln : pc.primaryNoNewArray_lf_primary()) {
                                processPrimaryNoNewArraylfprimaryContext(pc, pln, member);

                            }
                        }
                    }

                }

            }
        }
        return null;

    }

    private void processPrimaryNoNewArraylfprimaryContext(JavaParser.PrimaryContext pc, JavaParser.PrimaryNoNewArray_lf_primaryContext pnna, Member member) {
        if (
            pc.primaryNoNewArray_lfno_primary() != null
                && pc.primaryNoNewArray_lfno_primary().classInstanceCreationExpression_lfno_primary() != null
                && pc.primaryNoNewArray_lfno_primary().classInstanceCreationExpression_lfno_primary().Identifier() != null
                && pc.primaryNoNewArray_lfno_primary().classInstanceCreationExpression_lfno_primary().Identifier().size() == 1

            ) {

            String invokedClass = pc.primaryNoNewArray_lfno_primary().classInstanceCreationExpression_lfno_primary().Identifier().get(0).getText();
            graph.appendLogging("POINT A: " + pc.getText(), member);
            processMethodInvocation(pnna.methodInvocation_lf_primary().Identifier().getText(), pnna.methodInvocation_lf_primary().argumentList(), member, invokedClass, null);
        }
        if (
            pnna != null
            && pc.primaryNoNewArray_lfno_primary() != null
                && pc.primaryNoNewArray_lfno_primary().methodInvocation_lfno_primary() != null
                && pnna.methodInvocation_lf_primary() != null
                && pnna.methodInvocation_lf_primary().Identifier() != null
                && pc.primaryNoNewArray_lfno_primary().methodInvocation_lfno_primary().typeName() != null
                && pc.primaryNoNewArray_lfno_primary().methodInvocation_lfno_primary().typeName().Identifier() != null
            ) {
            String invokedClass = graph.findVariable(member.className, pc.primaryNoNewArray_lfno_primary().methodInvocation_lfno_primary().typeName().Identifier().getText());
            graph.appendLogging("POINT Z: " + pnna.getText(), member);
            processMethodInvocation(pnna.methodInvocation_lf_primary().Identifier().getText(), pnna.methodInvocation_lf_primary().argumentList(), member, invokedClass, null);

        }

        if (
            pc.primaryNoNewArray_lfno_primary() != null
                && pc.primaryNoNewArray_lfno_primary().classInstanceCreationExpression_lfno_primary() != null
                && pc.primaryNoNewArray_lfno_primary().classInstanceCreationExpression_lfno_primary().Identifier() != null
                && pc.primaryNoNewArray_lfno_primary().classInstanceCreationExpression_lfno_primary().Identifier().size() == 2

            ) {

            String invokedClass = "";
            for (TerminalNode terminalNode : pc.primaryNoNewArray_lfno_primary().classInstanceCreationExpression_lfno_primary().Identifier()) {
                if (StringUtils.isEmpty(invokedClass)) {
                    invokedClass = terminalNode.getText();
                } else {
                    invokedClass = invokedClass + "." + terminalNode.getText();
                }
            }

            //.doSomethingElse() ends up here
            graph.appendLogging("POINT B: " + pc.getText(), member);
            processMethodInvocation(pnna.methodInvocation_lf_primary().Identifier().getText(), pnna.methodInvocation_lf_primary().argumentList(), member, invokedClass, null);
        }
    }

    static boolean isLambaExpression(JavaParser.VariableDeclaratorContext variableDeclaratorContext) {
        return variableDeclaratorContext.variableInitializer() != null
            && variableDeclaratorContext.variableInitializer().expression() != null
            && variableDeclaratorContext.variableInitializer().expression().lambdaExpression() != null;
    }

    void logPreIncrementExpression(JavaParser.StatementExpressionContext sec, Member member) {
        if (sec.preIncrementExpression() != null) {
            graph.appendLogging("SEC.preIncrementExpression() " + sec.preIncrementExpression().getText(), member);
        }
    }

    void logPostIncrementExpression(JavaParser.StatementExpressionContext sec, Member member) {
        if (sec.postIncrementExpression() != null) {
            graph.appendLogging("SEC.postIncrementExpression() " + sec.postIncrementExpression().getText(), member);
        }
    }

    void logPostDecrementExpression(JavaParser.StatementExpressionContext sec, Member member) {
        if (sec.postDecrementExpression() != null) {
            graph.appendLogging("SEC.postDecrementExpression() " + sec.postDecrementExpression().getText(), member);
        }
    }

    void logClassInstanceCreationException(JavaParser.StatementExpressionContext sec, Member member) {
        if (sec.classInstanceCreationExpression() != null) {
            graph.appendLogging("SEC.classInstanceCreationExpression() " + sec.classInstanceCreationExpression().getText(), member);
        }
    }

    void logEmptyStatement(JavaParser.StatementWithoutTrailingSubstatementContext stwtsc, Member member) {
        if (stwtsc.emptyStatement() != null) {

            graph.appendLogging("STWTSC.emptyStatement() " + stwtsc.emptyStatement().getText(), member);
        }
    }

    void logAssertStatement(JavaParser.StatementWithoutTrailingSubstatementContext stwtsc, Member member) {
        if (stwtsc.assertStatement() != null) {

            graph.appendLogging("STWTSC.assertStatement() " + stwtsc.assertStatement().getText(), member);
        }
    }

    void logBreakStatement(JavaParser.StatementWithoutTrailingSubstatementContext stwtsc, Member member) {
        if (stwtsc.breakStatement() != null) {
            graph.appendLogging("STWTSC.breakStatement() " + stwtsc.breakStatement().getText(), member);

        }
    }

    void logContinueStatement(JavaParser.StatementWithoutTrailingSubstatementContext stwtsc, Member member) {
        if (stwtsc.continueStatement() != null) {
            graph.appendLogging("STWTSC.continueStatement() " + stwtsc.continueStatement().getText(), member);

        }
    }

    private void addAll(List<Member> originalList, List<Member> returnedList) {
        if (returnedList != null && !returnedList.isEmpty()) {
            originalList.addAll(returnedList.stream().filter(member -> member != null).collect(Collectors.toList()));
        }
    }
}
