package uk.ac.open.t802.network.walker.listener;

import uk.ac.open.t802.network.walker.domain.*;
import uk.ac.open.t802.network.walker.grammar.JavaParser;

/**
 * Author youlib
 * Since version 1.0
 */
public class ClassWalkerListener extends MemberBaseListener {

    private final Graph graph;
    private final MemberGraph memberGraph;
    private final DependencyGraph dependencyGraph;

    private final String className;
    private String deductedClassName;

    private String implementingInterface;

    public ClassWalkerListener(Graph graph, MemberGraph memberGraph, DependencyGraph dependencyGraph, String className) {
        this.graph = graph;
        this.memberGraph = memberGraph;
        this.dependencyGraph = dependencyGraph;
        this.className = className;
    }

    @Override
    public void enterEnumDeclaration(JavaParser.EnumDeclarationContext ctx) {
        if (ctx.Identifier() != null && !ctx.Identifier().getText().equals(className)) {
            deductedClassName = className + "." + ctx.Identifier().getText();
        } else if (ctx.Identifier() != null && ctx.Identifier().getText().equals(className)) {
            deductedClassName = className;
        }
    }

    @Override
    public void exitEnumDeclaration(JavaParser.EnumDeclarationContext ctx) {
        deductedClassName = className;

    }

    @Override
    public void enterNormalClassDeclaration(JavaParser.NormalClassDeclarationContext ctx) {
        if (ctx.Identifier() != null && !ctx.Identifier().getText().equals(className)) {
            dependencyGraph.graph.addVariable(className, ctx.Identifier().getText(), className + "." + ctx.Identifier().getText());
            deductedClassName = className + "." + ctx.Identifier().getText();
        } else if (ctx.Identifier() != null && ctx.Identifier().getText().equals(className)) {
            deductedClassName = className;
        }
        if (ctx.superclass() != null
            && ctx.superclass().classType() != null
            && ctx.superclass().classType().Identifier() != null) {
            graph.addExtents(deductedClassName, ctx.superclass().classType().Identifier().getText());
        }

    }

    @Override
    public void exitNormalClassDeclaration(JavaParser.NormalClassDeclarationContext ctx) {
        deductedClassName = className;
    }

    @Override
    public void enterInterfaceType(JavaParser.InterfaceTypeContext ctx) {
        implementingInterface = ctx.classType().Identifier().getText();
        graph.addInterface(className, implementingInterface);
    }

    @Override
    public void enterMethodDeclaration(JavaParser.MethodDeclarationContext ctx) {

        Member member = processMethodDeclaration(ctx, deductedClassName, dependencyGraph.graph);
        if (member == null) {
            return;
        }
        dependencyGraph.addNode(new JavaClass(packageName, deductedClassName, Color.BLUE));
        memberGraph.addNode(member);

        if ((ctx.methodBody() != null) && (ctx.methodBody().block() != null) && (ctx.methodBody().block().blockStatements() != null)) {
            for (JavaParser.BlockStatementContext blockStatementContext : ctx.methodBody().block().blockStatements().blockStatement()) {
                if (blockStatementContext.localVariableDeclarationStatement() != null) {
                    String type = blockStatementContext.localVariableDeclarationStatement().localVariableDeclaration().unannType().getText();
                    String variableName = blockStatementContext.localVariableDeclarationStatement().localVariableDeclaration().variableDeclaratorList().variableDeclarator().get(0).variableDeclaratorId().getText();
                    dependencyGraph.graph.addVariable(deductedClassName, variableName, type);
                }
                if (blockStatementContext.statement() != null
                    && blockStatementContext.statement().statementWithoutTrailingSubstatement() != null
                    && blockStatementContext.statement().statementWithoutTrailingSubstatement().expressionStatement() != null
                    && blockStatementContext.statement().statementWithoutTrailingSubstatement().expressionStatement().statementExpression() != null
                    && blockStatementContext.statement().statementWithoutTrailingSubstatement().expressionStatement().statementExpression().methodInvocation() != null
                    && blockStatementContext.statement().statementWithoutTrailingSubstatement().expressionStatement().statementExpression().methodInvocation().typeName() != null
                    && blockStatementContext.statement().statementWithoutTrailingSubstatement().expressionStatement().statementExpression().methodInvocation().typeName().Identifier() != null
                    && blockStatementContext.statement().statementWithoutTrailingSubstatement().expressionStatement().statementExpression().methodInvocation().typeName().Identifier().getText() != null
                    && blockStatementContext.statement().statementWithoutTrailingSubstatement().expressionStatement().statementExpression().methodInvocation().argumentList() != null
                    && blockStatementContext.statement().statementWithoutTrailingSubstatement().expressionStatement().statementExpression().methodInvocation().argumentList().expression() != null
                    ) {
                    //deliveryList
                    String invokedObjectVariable = blockStatementContext.statement().statementWithoutTrailingSubstatement().expressionStatement().statementExpression().methodInvocation().typeName().Identifier().getText();
                    for (JavaParser.ExpressionContext expressionContext : blockStatementContext.statement().statementWithoutTrailingSubstatement().expressionStatement().statementExpression().methodInvocation().argumentList().expression()) {
                        if (expressionContext.lambdaExpression() != null
                            && expressionContext.lambdaExpression().lambdaParameters() != null
                            && expressionContext.lambdaExpression().lambdaParameters().Identifier() != null
                            && expressionContext.lambdaExpression().lambdaParameters().Identifier().getText() != null) {
                            String typeParent = dependencyGraph.graph.findVariable(deductedClassName, invokedObjectVariable);
                            String type = ParseUtils.getType(typeParent);
                            String variableName = expressionContext.lambdaExpression().lambdaParameters().Identifier().getText();
                            dependencyGraph.graph.addVariable(deductedClassName, variableName, type);
                        }
                    }

                }
            }

        }

    }

    @Override
    public void enterClassBodyDeclaration(JavaParser.ClassBodyDeclarationContext ctx) {

        if (ctx.classMemberDeclaration() != null && ctx.classMemberDeclaration().fieldDeclaration() != null) {
            String type = ctx.classMemberDeclaration().fieldDeclaration().unannType().getText();
            ctx.classMemberDeclaration().fieldDeclaration().variableDeclaratorList().variableDeclarator().forEach(variableDeclaratorContext -> {
                String variableName = variableDeclaratorContext.variableDeclaratorId().Identifier().getText();
                graph.addVariable(deductedClassName, variableName, type);

            });

        }

    }

    @Override
    public void enterLiteral(JavaParser.LiteralContext ctx) {
        if (ctx.CharacterLiteral() != null) {
            graph.addVariable(className, ctx.CharacterLiteral().getSymbol().getText(), "Char");

        }
        if (ctx.StringLiteral() != null) {
            graph.addVariable(className, ctx.StringLiteral().getSymbol().getText(), "String");
        }
        if (ctx.BooleanLiteral() != null) {
            graph.addVariable(className, ctx.BooleanLiteral().getSymbol().getText(), "Boolean");
        }
        if (ctx.FloatingPointLiteral() != null) {
            graph.addVariable(className, ctx.FloatingPointLiteral().getSymbol().getText(), "Float");
        }
        if (ctx.IntegerLiteral() != null) {
            graph.addVariable(className, ctx.IntegerLiteral().getSymbol().getText(), "Integer");
        }
        if (ctx.NullLiteral() != null) {
            graph.addVariable(className, ctx.NullLiteral().getSymbol().getText(), "Null");
        }

    }

}
