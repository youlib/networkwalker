package uk.ac.open.t802.network.walker.listener;

import uk.ac.open.t802.network.walker.domain.Graph;
import uk.ac.open.t802.network.walker.domain.Member;
import uk.ac.open.t802.network.walker.domain.MemberGraph;
import uk.ac.open.t802.network.walker.grammar.JavaParser;
import uk.ac.open.t802.network.walker.listener.context.ContextAnalyser;

/**
 * @author Georgia Bogdanou
 */
public class InterfaceMemberWalkerListener extends MemberBaseListener {

    private final Graph graph;
    private ContextAnalyser contextAnalyser;
    private final String interfaceName;

    public InterfaceMemberWalkerListener(Graph graph, MemberGraph memberGraph, MemberGraph resourcesGraph, String className) {
        this.graph = graph;
        this.interfaceName = className;
        this.contextAnalyser = new ContextAnalyser(graph, memberGraph, resourcesGraph);
    }


    @Override
    public void enterInterfaceMethodDeclaration(JavaParser.InterfaceMethodDeclarationContext ctx) {
        Member member = processMethodDeclaration(ctx, interfaceName, graph);
        if (member != null) {
            // a Method body can only have block statements, so this is fully explored
            if (ctx.methodBody() != null && ctx.methodBody().block() != null && ctx.methodBody().block().blockStatements() != null) {
                ctx.methodBody().block().blockStatements().blockStatement().forEach(statement -> contextAnalyser.processMethodInvocation(statement, member));
            }

        }
    }
}
