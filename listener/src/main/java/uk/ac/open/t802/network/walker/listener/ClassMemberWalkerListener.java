package uk.ac.open.t802.network.walker.listener;

import uk.ac.open.t802.network.walker.domain.Graph;
import uk.ac.open.t802.network.walker.domain.Member;
import uk.ac.open.t802.network.walker.domain.MemberGraph;
import uk.ac.open.t802.network.walker.grammar.JavaParser;
import uk.ac.open.t802.network.walker.listener.context.ContextAnalyser;

/**
 * Author youlib
 * Since version 1.0
 */
public class ClassMemberWalkerListener extends MemberBaseListener {

    private final Graph graph;
    private ContextAnalyser contextAnalyser;
    private final String className;
    private String deductedClassName;

    public ClassMemberWalkerListener(Graph graph, MemberGraph memberGraph, MemberGraph resourcesGraph, String className) {
        this.graph = graph;
        this.className = className;
        this.contextAnalyser = new ContextAnalyser(graph, memberGraph, resourcesGraph);
    }

    @Override
    public void enterEnumDeclaration(JavaParser.EnumDeclarationContext ctx) {
        if (ctx.Identifier() != null && !ctx.Identifier().getText().equals(className)) {
            deductedClassName = className + "." + ctx.Identifier().getText();
        } else if (ctx.Identifier() != null && ctx.Identifier().getText().equals(className)) {
            deductedClassName = className;
        }
    }

    @Override
    public void exitEnumDeclaration(JavaParser.EnumDeclarationContext ctx) {
        deductedClassName = className;

    }

    @Override
    public void enterNormalClassDeclaration(JavaParser.NormalClassDeclarationContext ctx) {
        deductedClassName = deductClassName(ctx, className);
    }

    @Override
    public void exitNormalClassDeclaration(JavaParser.NormalClassDeclarationContext ctx) {
        deductedClassName = className;
    }

    @Override
    public void enterMethodDeclaration(JavaParser.MethodDeclarationContext ctx) {
        Member member = processMethodDeclaration(ctx, deductedClassName, graph);
        if (member != null) {
            // a Method body can only have block statements, so this is fully explored
            if (ctx.methodBody() != null && ctx.methodBody().block() != null && ctx.methodBody().block().blockStatements() != null) {
                ctx.methodBody().block().blockStatements().blockStatement().forEach(statement -> contextAnalyser.processMethodInvocation(statement, member));
            }

        }
    }

}